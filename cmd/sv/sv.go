package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/oklog/run"

	"sv.app/internal/config"
	"sv.app/internal/instrumentation"
	"sv.app/internal/logger"
	"sv.app/internal/manager"
	svos "sv.app/internal/os"
	"sv.app/internal/rpc"
	"sv.app/internal/support"
)

// Version is application version string
var Version = "dev"

func main() {
	f := flag.NewFlagSet("config", flag.ExitOnError)
	f.String("conf", "sv.toml", "Configuration file name")
	f.Bool("debug", false, "Enable debug mode")
	loglevel := f.String("log.level", "info", "Logging level (debug, info, warn, error, fatal)")
	logformat := f.String("log.format", "logfmt", "Logging log format (logfmt, json).")

	f.Parse(os.Args[1:])

	values := f.Args()

	if len(values) == 0 {
		fmt.Fprintln(os.Stderr, "argument required...")
		return
	}

	logger.InitializeLogger(*loglevel, *logformat)
	conf := config.Initialize(f)
	if err := config.Load(conf); err != nil {
		panic(err)
	}
	logger.Logger.Debug().Interface("conf", conf).Msg("conf loaded")

	if values[0] == "serve" {
		startDaemon(conf)
	} else {
		startCommand(conf, values)
	}
}

func startCommand(conf *config.Config, args []string) {
	logger.Logger.Debug().Msgf("startCommand: %#v", args)
	cl := rpc.Client{Conf: conf}
	if err := cl.Call(args[0], args[1:]); err != nil {
		panic(err)
	}
}

func startDaemon(conf *config.Config) {
	l := logger.Logger.With().Str("ctx", "daemon").Logger()
	ctx := l.WithContext(context.Background())

	// start app
	l.Info().Msg("Starting...")

	var g run.Group
	broker := support.NewBroker()
	mgm := manager.NewManager(conf, broker)

	{
		stop := make(chan os.Signal, 1)
		signal.Notify(stop, os.Interrupt)
		signal.Notify(stop, syscall.SIGTERM)
		cancel := make(chan struct{})
		g.Add(func() error {
			select {
			case <-stop:
				mgm.ManagerShutdown(ctx)
				l.Info().Msg("interrupt")
			case <-cancel:
				break
			}
			return nil
		}, func(err error) {
			close(cancel)
		})
	}

	{
		g.Add(broker.Start, func(err error) {
			l.Info().Err(err).Msg("broker stopping")
			broker.Stop()
		})
	}

	{
		reload := make(chan os.Signal, 1)
		signal.Notify(reload, syscall.SIGHUP)
		cancel := make(chan struct{})
		g.Add(func() error {
			lsnr := broker.Subscribe()
			defer broker.Unsubscribe(lsnr)
			for {
				select {
				case <-reload:
					mgm.Action(ctx, &manager.ActionReq{
						Action: manager.ActionReloadConf,
					})
				case <-cancel:
					return nil
				}
			}
		}, func(err error) {
			close(cancel)
		})
	}

	{
		server := rpc.NewServer(mgm, conf)
		g.Add(server.Start, func(err error) {
			l.Error().Err(err).Msg("server stopping")
			server.Stop()
		})
	}

	if conf.MetricsAddress != "" {
		mserver := instrumentation.NewMetricsServer(mgm, conf)
		g.Add(mserver.Start, func(err error) {
			l.Error().Err(err).Msg("metrics server stopping")
			mserver.Stop()
		})
	}

	{
		g.Add(mgm.Start, func(err error) {
			l.Info().Err(err).Msg("manager stopping")
			mgm.Stop()
		})
	}

	go func() {
		time.Sleep(1 * time.Second)
		mgm.AutoStart(ctx)
	}()

	svos.StartReaper()

	err := g.Run()
	l.Info().Err(err).Msg("application terminated")
}
