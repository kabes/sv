#
# Makefile
# Karol Będkowski, 2021-11-20 09:48
#

build:
	go build

serve: build
	./sv.app -debug --log.level debug serve



# vim:ft=make
#
