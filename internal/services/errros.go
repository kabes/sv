package services

import "errors"

//
// errros.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

// ErrProcessNotRunning is "process not running error"
var ErrProcessNotRunning = errors.New("process not running")
