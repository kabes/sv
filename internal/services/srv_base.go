package services

//
// base_service.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/rs/zerolog"

	"sv.app/internal/config"
	"sv.app/internal/logger"
	"sv.app/internal/support"
)

// baseService implement common parts of services that can but not must be used in service
// implementation
type baseService struct {
	conf   *config.ServiceConf
	broker *support.Broker

	// stdin     io.WriteCloser
	log zerolog.Logger
}

func newBaseService(conf *config.ServiceConf, broker *support.Broker) baseService {
	return baseService{
		conf:   conf,
		broker: broker,

		log: logger.Logger.With().Str("service", conf.Name).Logger(),
	}
}

func (s *baseService) sendEvent(ctx context.Context, eventType EventType) {
	zerolog.Ctx(ctx).Trace().Interface("eventType", eventType).Caller().Msg("send event")
	s.broker.Publish(NewInternalEvent(s.conf.Name, eventType))
}

func (s *baseService) sendEventMsg(ctx context.Context, eventType EventType, msg string) {
	zerolog.Ctx(ctx).Trace().Interface("eventType", eventType).
		Str("msg", msg).Caller().Msg("send event")
	s.broker.Publish(NewInternalEvent(s.conf.Name, eventType, msg))
}

func (s *baseService) waitFor(ctx context.Context, timeout uint, statuses ...Status) (bool, Status) {
	if timeout == 0 {
		return true, StatusUnknown
	}

	s.log.Debug().Interface("statuses", statuses).Msg("wait for...")

	ctx, cancel := context.WithTimeout(ctx, time.Duration(timeout)*time.Second)
	defer cancel()

	notif := s.broker.Subscribe()
	defer s.broker.Unsubscribe(notif)

	for {
		select {
		case <-ctx.Done():
			s.log.Debug().Interface("wait_for", statuses).Msg("wait for status timeout")
			return false, StatusUnknown
		case n := <-notif:
			if event, ok := n.(*StatusEvent); ok && event.Service == s.conf.Name {
				for _, st := range statuses {
					if event.Status == st {
						return true, st
					}
				}
			}
		}

	}
}

func (s *baseService) ReloadLogger(ctx context.Context) error {
	if l := s.conf.LogStdout; l != "" {
		if err := globalServiceLoggers.Reload(l); err != nil {
			return fmt.Errorf("reload %s error: %w", l, err)
		}
	}
	if l := s.conf.LogStderr; l != "" && l != s.conf.LogStdout {
		if err := globalServiceLoggers.Reload(l); err != nil {
			return fmt.Errorf("reload %s error: %w", l, err)
		}
	}
	return nil
}

func (s *baseService) Logs(ctx context.Context, output string, offset int64, buf *[]byte) (int64, error) {
	var logFilename string
	switch output {
	case "stdout":
		logFilename = s.conf.LogStdout
	case "stderr":
		logFilename = s.conf.LogStderr
	default:
		logFilename = s.conf.LogStdout
	}

	if logFilename == "" {
		return 0, fmt.Errorf("log file for %s not available", output)
	}

	file, err := os.Open(logFilename)
	if err != nil {
		return 0, fmt.Errorf("open file error: %w", err)
	}

	defer file.Close()

	if offset == 0 {
		offset, err = file.Seek(int64(-len(*buf)), 2)
		if err != nil {
			return 0, fmt.Errorf("seek file error: %w", err)
		}
	}

	length, err := file.ReadAt(*buf, offset)
	if err == io.EOF {
		// when eof return all read part at once
		*buf = (*buf)[:length]
		return offset + int64(length), err
	} else if err != nil {
		return 0, fmt.Errorf("read file error: %w", err)
	}

	// trim buf to newline
	if lastNl := bytes.LastIndexByte(*buf, '\n'); lastNl > 0 {
		length = lastNl
	}

	*buf = (*buf)[:length]

	return offset + int64(length), nil
}
