package services

import (
	"context"
	"fmt"

	"sv.app/internal/config"
	"sv.app/internal/support"
)

//
// mod.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

// ServiceImpl must be implement by all services
type ServiceImpl interface {
	// Check is internal service healtcheck; should quick check is service is alive.
	// Is called every 1 sec
	Check(ctx context.Context) error
	// start service; this func should not block
	Start(ctx context.Context) error
	// stop stop service; this func should not block
	Stop(ctx context.Context) error
	Status(ctx context.Context, verbose int) *StatusInfo
	Reload(ctx context.Context) error

	ReloadLogger(ctx context.Context) error
	Logs(ctx context.Context, output string, offset int64, buf *[]byte) (int64, error)

	ShouldAutoStart() (autostart bool)
	ShouldAutoRestart(attempt uint) (autorestart bool, delay uint)
}

// ServiceImplProcess must be implement by services that control process directly
type ServiceImplProcess interface {
	Pid(ctx context.Context) (int, error)
	Signal(ctx context.Context, signal string) error
}

// NewService create new service implementation according to configuration
func NewService(conf *config.ServiceConf, broker *support.Broker) (ServiceImpl, error) {
	switch conf.Type {
	case config.ServiceTypeNormal:
		return newServiceBackground(conf, broker), nil
	case config.ServiceTypeExt:
		return newServiceExternal(conf, broker), nil
	default:
	}

	return nil, fmt.Errorf("unknown/not supported service type: %s", conf.Type)
}
