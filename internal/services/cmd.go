package services

//
// cmd.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"syscall"
	"time"

	"github.com/google/shlex"
	"github.com/rs/xid"
	"github.com/rs/zerolog"

	"sv.app/internal/logger"
	svos "sv.app/internal/os"
)

// Command run given command
type Command struct {
	Command     string
	Environment []string
	Directory   string
	Stdout      string
	Stderr      string

	cmd *exec.Cmd
	id  xid.ID
	log zerolog.Logger
}

// Prepare command to run (setup, open std err/out)
func (c *Command) Prepare(ctx context.Context) error {
	if c.cmd != nil {
		return fmt.Errorf("command already prepared")
	}

	commandParts, err := shlex.Split(c.Command)
	if err != nil {
		return err
	}

	c.cmd = exec.CommandContext(ctx, commandParts[0])
	if len(commandParts) > 1 {
		c.cmd.Args = commandParts
	}
	c.cmd.Env = os.Environ()
	if s := svos.GetSysProcAttr(); s != nil {
		c.cmd.SysProcAttr = s
	}
	// if s.stdin, err = cmd.StdinPipe(); err != nil {
	// 	return nil, err
	// }

	if err := c.setStdOut(c.Stdout); err != nil {
		return err
	}

	if err := c.setStdErr(c.Stderr); err != nil {
		// close stdout if opened
		c.closeStdouterr()
		return err
	}

	if len(c.Environment) > 0 {
		c.cmd.Env = append(os.Environ(), c.Environment...)
	}
	if c.Directory != "" {
		c.cmd.Dir = c.Directory
	}

	c.id = xid.New()
	c.log = logger.Logger.With().Stringer("cmd_id", c.id).Logger()
	c.log.Debug().Interface("cmd", c).Msg("command prepared")

	return nil
}

func (c *Command) setStdOut(logname string) error {
	if logname == "" {
		return nil
	}
	stdout, err := globalServiceLoggers.Open(logname, "")
	if err != nil {
		return fmt.Errorf("open stdout (%s) error: %w", logname, err)
	}

	c.cmd.Stdout = stdout
	return nil
}

func (c *Command) setStdErr(logname string) error {
	if logname == "" {
		return nil
	}
	stdout, err := globalServiceLoggers.Open(logname, "")
	if err != nil {
		return fmt.Errorf("open stderr (%s) error: %w", logname, err)
	}

	c.cmd.Stderr = stdout
	return nil
}

// Run command; return result or error
func (c *Command) Run() (string, error) {
	c.log.Debug().Msg("run command")
	err := c.cmd.Run()

	c.log.Debug().Err(err).Msg("command finished")

	c.closeStdouterr()

	res := ""
	if ps := c.cmd.ProcessState; ps != nil {
		if !ps.Success() {
			if err != nil {
				err = fmt.Errorf("command failed: %s", ps.String())
			}
		} else {
			res = ps.String()
		}
	}

	c.cmd = nil
	return res, err
}

// PID return pid of running process
func (c *Command) PID() (int, bool) {
	if c.cmd == nil || c.cmd.Process == nil {
		return 0, false
	}

	return c.cmd.Process.Pid, true
}

// Running check is process still running
func (c *Command) Running() bool {
	if c.cmd == nil || c.cmd.Process == nil {
		return false
	}
	if ps := c.cmd.ProcessState; ps == nil {
		if c.cmd.Process.Signal(syscall.Signal(0)) == nil {
			return true
		}
	}

	return false
}

// Kill process with signal & check status after 1 sec
func (c *Command) Kill(signal os.Signal) error {
	c.log.Debug().Msgf("kill command; signal=%v", signal)

	if err := c.Signal(signal, true); err != nil {
		return err
	}

	time.Sleep(1 * time.Second)
	if ps := c.cmd.ProcessState; ps == nil || ps.Exited() {
		return nil
	}

	return errors.New("kill failed")
}

// Signal send signal to process and optionally children
func (c *Command) Signal(signal os.Signal, sigChildren bool) error {
	c.log.Debug().Msgf("signal command; signal=%v", signal)

	if c.cmd == nil || c.cmd.Process == nil {
		return fmt.Errorf("missing process")
	}

	pid := c.cmd.Process.Pid
	if sigChildren {
		pid = -pid
	}

	proc, err := os.FindProcess(pid)
	if err == nil {
		err = proc.Signal(signal.(syscall.Signal))
	}
	return err
}

func (c *Command) closeStdouterr() {
	c.log.Trace().Msgf("closeStdouterr: %s, %s", c.Stdout, c.Stderr)

	if c.cmd.Stdout != nil && c.Stdout != "" {
		if err := globalServiceLoggers.Close(c.Stdout); err != nil {
			logger.Logger.Error().Err(err).Msg("close stdout error")
		}
	}
	if c.cmd.Stderr != nil && c.Stderr != "" {
		if err := globalServiceLoggers.Close(c.Stderr); err != nil {
			logger.Logger.Error().Err(err).Msg("close stderr error")
		}
	}
}
