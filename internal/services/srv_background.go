package services

// srv_background.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.

import (
	"context"
	"errors"
	"fmt"
	"syscall"

	"github.com/rs/zerolog"
	"sv.app/internal/config"
	"sv.app/internal/os"
	"sv.app/internal/support"
)

type serviceBackground struct {
	baseService

	exitMessage string

	cmd *Command
}

func newServiceBackground(conf *config.ServiceConf, broker *support.Broker) ServiceImpl {
	return &serviceBackground{
		baseService: newBaseService(conf, broker),
		exitMessage: "",
		cmd:         nil,
	}
}

func (s *serviceBackground) Start(ctx context.Context) error {
	l := zerolog.Ctx(ctx)

	// start process in background
	s.exitMessage = ""

	l.Debug().Str("cmd", s.conf.StartCommand).Msg("create command")
	s.cmd = &Command{
		Command:     s.conf.StartCommand,
		Environment: s.conf.Environment,
		Directory:   s.conf.Directory,
		Stdout:      s.conf.LogStdout,
		Stderr:      s.conf.LogStderr,
	}
	if err := s.cmd.Prepare(context.Background()); err != nil {
		return fmt.Errorf("create command error: %w", err)
	}

	l.Info().Msg("service starting...")
	s.broker.Publish(NewInternalEvent(s.conf.Name, EventServiceStarting))

	go func() {
		res, err := s.cmd.Run()
		s.cmd = nil
		l.Info().Err(err).Msgf("process exited: %s", res)
		if err != nil {
			s.exitMessage = err.Error()
		} else {
			s.exitMessage = res
		}

		s.sendEventMsg(ctx, EventProcessTerminated, "process terminated")
	}()

	return nil
}

func (s *serviceBackground) Stop(ctx context.Context) error {
	// stop service in background
	if s.cmd == nil {
		return nil
	}

	l := zerolog.Ctx(ctx)

	// wait for stop
	if s.conf.StopTimeout > 0 {
		go func() {
			if ok, status := s.waitFor(ctx, s.conf.StopTimeout, StatusStopped); ok {
				l.Info().Interface("status", status).Msg("service stopped")
				return
			}

			s.log.Warn().Str("action", "stop").Msg("service failed to stop before timeout")
			// try kill process
			if err := s.cmd.Kill(syscall.SIGKILL); err == nil {
				l.Info().Msg("process stopped by kill")
				s.exitMessage = "killed"
				s.sendEventMsg(ctx, EventProcessStopped, "process killed")
			} else {
				l.Error().Err(err).Msg("stopping service by kill failed")
				s.exitMessage = err.Error()
				s.sendEventMsg(ctx, EventServiceFailedStop, "failed to kill process")
			}
		}()
	}

	sig, _ := os.GetSignal(s.conf.StopSignal)
	if err := s.cmd.Signal(sig, false); err != nil {
		return fmt.Errorf("failed to stop process: %w", err)
	}

	return nil
}

func (s *serviceBackground) Status(ctx context.Context, verbose int) *StatusInfo {
	res := NewServiceStatus(s.conf.Name)
	if verbose > 0 {
		res.Add("exit_message", s.exitMessage)
		if s.cmd != nil {
			if pid, ok := s.cmd.PID(); ok {
				res.Data["pid"] = fmt.Sprintf("%d", pid)
			}
		}
	}

	return res
}

func (s *serviceBackground) Signal(ctx context.Context, signame string) error {
	signal, ok := os.GetSignal(signame)
	if !ok {
		return errors.New("unknown signal")
	}

	if err := s.cmd.Signal(signal, false); err != nil {
		return fmt.Errorf("signal error: %w", err)
	}

	return nil
}

func (s *serviceBackground) Pid(ctx context.Context) (int, error) {
	if s.cmd != nil {
		if pid, ok := s.cmd.PID(); ok {
			return pid, nil
		}
	}

	return 0, errors.New("process not running")
}

func (s *serviceBackground) Reload(ctx context.Context) error {
	signame := s.conf.ReloadSignal
	signal, ok := os.GetSignal(signame)
	if !ok {
		return fmt.Errorf("unknown signal: %s", signame)
	}

	if s.cmd != nil {
		if err := s.cmd.Signal(signal, false); err != nil {
			return fmt.Errorf("signal error: %w", err)
		}
	}

	return nil
}

func (s *serviceBackground) Check(ctx context.Context) error {
	if s.cmd != nil && s.cmd.Running() {
		return nil
	}
	return errors.New("missing process")
}

func (s *serviceBackground) ShouldAutoStart() (autostart bool) {
	return s.conf.AutoStart
}

func (s *serviceBackground) ShouldAutoRestart(attempt uint) (autorestart bool, delay uint) {
	if !s.conf.AutoRestart {
		return false, 0
	}

	if s.conf.MaxAutoRestart > 0 && attempt < s.conf.MaxAutoRestart {
		return true, s.conf.RestartDelay
	}

	return false, 0
}
