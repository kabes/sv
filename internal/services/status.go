package services

import (
	"time"
)

// services.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.

// Status of service
type Status string

// InProgress return true when service is in "progress"
func (s Status) InProgress() bool {
	return s == StatusStarting || s == StatusStopping
}

// SubStatus for service
type SubStatus string

const (
	// StatusStopped = service is stopped
	StatusStopped Status = "stopped"
	// StatusStarting = service is now starting (got start action)
	StatusStarting Status = "starting"
	// StatusRunning = service is now running correctly
	StatusRunning Status = "running"
	// StatusStopping = service is now stopping (got stop action)
	StatusStopping Status = "stopping"
	// StatusWaiting = status waiting i.e. for restart
	StatusWaiting Status = "waiting"
	// StatusUnknown is returned when status cannot be determined
	StatusUnknown Status = "unknown"

	// SubStatusNone is default substatus
	SubStatusNone SubStatus = ""
	// SubStatusBroken is substaus for stopped and failed service
	SubStatusBroken SubStatus = "broken"
)

// StatusInfo is result of status action for one service
type StatusInfo struct {
	Service   string
	Status    Status
	SubStatus SubStatus
	Data      map[string]string
}

// NewServiceStatus create new StatusInfo for service
func NewServiceStatus(service string) *StatusInfo {
	return &StatusInfo{
		Service: service,
		Data:    make(map[string]string),
	}
}

// StatusHuman return formatted status
func (s *StatusInfo) StatusHuman() string {
	if s.SubStatus == SubStatusNone {
		return string(s.Status)
	}
	return string(s.Status) + " / " + string(s.SubStatus)
}

// AddDate add not-empty timestamp to Data
func (s *StatusInfo) AddDate(key string, t time.Time) {
	if !t.IsZero() {
		s.Data[key] = t.Format(time.RFC3339)
	}
}

// Add add val to Data if val is not empty
func (s *StatusInfo) Add(key, val string) {
	if val != "" {
		s.Data[key] = val
	}
}

// AddWarning add not-empty 'warning' message
func (s *StatusInfo) AddWarning(msg string) {
	if msg != "" {
		s.Data["warning"] = msg
	}
}

// AddData add not-empty message under `key`
func (s *StatusInfo) AddData(key, msg string) {
	if msg != "" {
		s.Data[key] = msg
	}
}
