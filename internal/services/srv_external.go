package services

//
// service_ext.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//
// FIXME: różnowległe zapisy do logu przy równoległych cmd

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/rs/zerolog"
	"sv.app/internal/config"
	"sv.app/internal/support"
)

type serviceExternal struct {
	baseService

	exitMessage string
}

func newServiceExternal(conf *config.ServiceConf, broker *support.Broker) ServiceImpl {
	return &serviceExternal{
		baseService: newBaseService(conf, broker),
		exitMessage: "",
	}
}

func (s *serviceExternal) Start(ctx context.Context) error {
	// start process in background
	s.exitMessage = ""

	var cancel context.CancelFunc
	if s.conf.StartTimeout > 0 {
		ctx, cancel = context.WithTimeout(ctx, time.Duration(s.conf.StartTimeout)*time.Second)
	}

	cmd := Command{
		Command:     s.conf.StartCommand,
		Environment: s.conf.Environment,
		Directory:   s.conf.Directory,
		Stdout:      s.conf.LogStdout,
		Stderr:      s.conf.LogStderr,
	}
	if err := cmd.Prepare(ctx); err != nil {
		if cancel != nil {
			cancel()
		}
		s.exitMessage = "create command error: " + err.Error()
		return fmt.Errorf("create command error: %v", err)
	}

	l := zerolog.Ctx(ctx)
	l.Info().Msg("starting service")

	s.broker.Publish(NewInternalEvent(s.conf.Name, EventServiceStarting))

	go func() {
		res, err := cmd.Run()
		if cancel != nil {
			cancel()
		}

		if err != nil {
			s.log.Error().Err(err).Msg("start command failed")
			s.exitMessage = "start command failed: " + err.Error()
			s.sendEventMsg(ctx, EventProcessFailedStart, "start failed")
			return
		}
		s.exitMessage = res
		l.Info().Msg("start command finished")
	}()

	return nil

}
func (s *serviceExternal) Stop(ctx context.Context) error {
	var cancel context.CancelFunc
	if s.conf.StopTimeout > 0 {
		ctx, cancel = context.WithTimeout(ctx, time.Duration(s.conf.StopTimeout)*time.Second)
	}

	cmd := Command{
		Command:     s.conf.StopCommand,
		Environment: s.conf.Environment,
		Directory:   s.conf.Directory,
		Stdout:      s.conf.LogStdout,
		Stderr:      s.conf.LogStderr,
	}
	if err := cmd.Prepare(ctx); err != nil {
		if cancel != nil {
			cancel()
		}
		return fmt.Errorf("create stop command error: %v", err)
	}

	l := zerolog.Ctx(ctx)
	l.Info().Msg("stopping service")

	// stop service in background
	go func() {
		s.log.Trace().Msg("execute stop command")
		defer func() {
			if cancel != nil {
				cancel()
			}
		}()

		res, err := cmd.Run()
		if err != nil {
			l.Error().Err(err).Stack().Msg("stopping service by script failed")
			s.sendEventMsg(ctx, EventServiceFailedStop, "stop service failed: "+err.Error())
			s.exitMessage = err.Error()
			return
		}

		s.exitMessage = res

		// FIXME: check
		// l.Info().Msg("service stopped")
		//s.sendEventMsg(ctx, EventProcessStopped, "service stopped")
	}()

	return nil
}

func (s *serviceExternal) Status(ctx context.Context, verbose int) *StatusInfo {
	res := NewServiceStatus(s.conf.Name)

	if verbose > 1 {
		res.AddData("exit_message", s.exitMessage)
	}

	return res
}

func (s *serviceExternal) Reload(ctx context.Context) error {
	c := s.conf.ReloadCommand
	if c == "" {
		return errors.New("reload command not defined")
	}

	var cancel context.CancelFunc
	if s.conf.ReloadTimeout > 0 {
		ctx, cancel = context.WithTimeout(ctx, time.Duration(s.conf.ReloadTimeout)*time.Second)
	}

	cmd := Command{
		Command:     c,
		Environment: s.conf.Environment,
		Directory:   s.conf.Directory,
		Stdout:      s.conf.LogStdout,
		Stderr:      s.conf.LogStderr,
	}
	if err := cmd.Prepare(ctx); err != nil {
		if cancel != nil {
			cancel()
		}
		return fmt.Errorf("create command error: %w", err)
	}

	l := zerolog.Ctx(ctx)
	l.Info().Msg("reloading service")

	// reload service in background
	go func() {
		defer func() {
			if cancel != nil {
				cancel()
			}
		}()

		res, err := cmd.Run()
		if err != nil {
			l.Error().Err(err).Msg("reloading service by script failed")
			s.sendEventMsg(ctx, EventServiceFailedReload, "reload failed: "+err.Error())
			return
		}

		l.Info().Msgf("service reloaded: %s", res)
		s.sendEventMsg(ctx, EventServiceReload, "service reloaded")
	}()

	return nil
}

func (s *serviceExternal) Check(ctx context.Context) error {
	return nil
}

func (s *serviceExternal) ShouldAutoStart() (autostart bool) {
	return s.conf.AutoStart
}

func (s *serviceExternal) ShouldAutoRestart(attempt uint) (autorestart bool, delay uint) {
	if !s.conf.AutoRestart {
		return false, 0
	}

	if s.conf.MaxAutoRestart > 0 && attempt < s.conf.MaxAutoRestart {
		return true, s.conf.RestartDelay
	}

	return false, 0
}
