package services

//
// logger.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"fmt"
	"sync"

	"github.com/rs/zerolog"
	"sv.app/internal/logger"
)

var globalServiceLoggers = &serviceLoggers{
	loggers: make(map[string]logger.ServiceLogger),
}

type serviceLoggers struct {
	sync.Mutex

	loggers map[string]logger.ServiceLogger
}

// Open logger by name for service
func (s *serviceLoggers) Open(logfile, service string) (logger.ServiceLogger, error) {
	s.Lock()
	defer s.Unlock()

	if l, ok := s.loggers[logfile]; ok {
		err := l.Open()
		logger.Logger.Trace().Err(err).Interface("logger", l).Msg("open log file - reuse")
		s.logDebufInfo()
		return l, err
	}

	l := logger.NewServiceFileLogger(logfile)

	logger.Logger.Trace().Interface("logger", l).Msg("open log file - create")

	err := l.Open()
	if err != nil {
		return nil, err
	}

	s.loggers[logfile] = l
	s.logDebufInfo()
	return l, nil
}

// Close logger by name
func (s *serviceLoggers) Close(logfile string) error {
	s.Lock()
	defer s.Unlock()

	l, ok := s.loggers[logfile]
	if !ok {
		return fmt.Errorf("unknown logfile: %s", logfile)
	}

	realyClosed, err := l.Close()
	if realyClosed {
		logger.Logger.Trace().Err(err).Interface("logger", l).Msg("close & delete log file")
		delete(s.loggers, logfile)
	} else {
		logger.Logger.Trace().Err(err).Interface("logger", l).Msg("close log file")
	}

	s.logDebufInfo()
	return err
}

func (s *serviceLoggers) Reload(logfile string) error {
	s.Lock()
	defer s.Unlock()

	if l, ok := s.loggers[logfile]; ok {
		logger.Logger.Trace().Interface("sl", l).Msg("reopen log file")
		return l.Reopen()
	}

	return fmt.Errorf("unknown logfile: %s", logfile)
}

func (s *serviceLoggers) logDebufInfo() {
	t := logger.Logger.Trace()
	if !t.Enabled() {
		return
	}

	t.Func(func(e *zerolog.Event) {
		for n, l := range s.loggers {
			e.Interface(n, l)
		}
	}).Msg("logger in pool")
}
