package manager

//
// healthchecks.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"bytes"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"time"

	// . "github.com/WAY29/icecream-go/icecream"
	"github.com/google/shlex"
	"github.com/rs/zerolog"

	"sv.app/internal/config"
)

const (
	minInterval int = 5
	minTimeout  int = 0
)

func intAsDuration(v int, minVal int) time.Duration {
	if v > minVal {
		return time.Duration(v) * time.Second
	}
	return time.Duration(minVal) * time.Second
}

// healthCheck define checker
type healthCheck interface {
	// check return true when it succeed
	check(ctx context.Context) (checked bool, err error)

	zerolog.LogObjectMarshaler
}

// processNameCheck check is process with given name exists
type processNameCheck struct {
	// process command line matcher
	matcher *regexp.Regexp
	// minimal number of process
	minNumProcess uint
	interval      time.Duration
	nextCheck     time.Time
}

func newProcessNameCheck(c *config.HCNamedProcessConf) (healthCheck, error) {
	re, err := regexp.Compile(c.Name)
	if err != nil {
		return nil, fmt.Errorf("compile matcher error: %w", err)
	}
	var numProc uint = 1
	if c.MinNum > 1 {
		numProc = uint(c.MinNum)
	}

	return &processNameCheck{
		matcher:       re,
		minNumProcess: numProc,
		interval:      intAsDuration(c.Interval, minInterval),
	}, nil
}

func (p *processNameCheck) check(ctx context.Context) (checked bool, err error) {
	if time.Now().Before(p.nextCheck) {
		return false, nil
	}

	l := zerolog.Ctx(ctx).With().Str("check", "processNameCheck").Logger()
	l.Debug().Msg("start")

	var found uint = 0

	files, _ := filepath.Glob("/proc/*/cmdline")
	for _, file := range files {
		cmdline, err := os.ReadFile(file)
		if err != nil {
			continue
		}

		cmdline = bytes.ReplaceAll(cmdline, []byte{0}, []byte{' '})
		if p.matcher.Match(cmdline) {
			found++
			if found >= p.minNumProcess {
				break
			}
		}
	}

	l.Debug().Uint("found", found).Msg("scan finished")
	if found < p.minNumProcess {
		err = fmt.Errorf("found %d process; expected %d", found, p.minNumProcess)
	}

	p.nextCheck = time.Now().Add(p.interval)
	return true, err
}

func (p *processNameCheck) MarshalZerologObject(e *zerolog.Event) {
	e.Str("name", "processNameCheck").
		Interface("matcher", p.matcher).
		Uint("minNumProcess", p.minNumProcess).
		Dur("interval", p.interval).
		Time("nextCheck", p.nextCheck)
}

// scriptCheck monitor is process running
type scriptCheck struct {
	script      string
	interval    time.Duration
	nextCheck   time.Time
	timeout     time.Duration
	environment []string
	directory   string
}

func newScriptCheck(c *config.HCScriptConf) healthCheck {
	s := &scriptCheck{
		script:      c.Command,
		nextCheck:   time.Now(),
		interval:    intAsDuration(c.Interval, minInterval),
		timeout:     intAsDuration(c.Timeout, minTimeout),
		directory:   c.Directory,
		environment: c.Environment,
	}

	return s
}

func (s *scriptCheck) check(ctx context.Context) (checked bool, err error) {
	if time.Now().Before(s.nextCheck) {
		return false, nil
	}

	l := zerolog.Ctx(ctx).With().Str("check", "scriptCheck").Logger()
	l.Debug().Msg("healthcheck start")

	scmd, err := shlex.Split(s.script)
	if err != nil {
		return false, fmt.Errorf("prepare command error: %w", err)
	}

	ctx = l.WithContext(ctx)

	if s.timeout > 0 {
		l.Trace().Dur("timeout", s.timeout).Msg("setup timeout")
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, s.timeout)
		defer cancel()
	}

	cmd := exec.CommandContext(ctx, scmd[0]) // nolint
	if len(scmd) > 1 {
		cmd.Args = scmd
	}

	if len(s.environment) > 0 {
		l.Trace().Strs("env", s.environment).Msg("setup environment")
		cmd.Env = append(os.Environ(), s.environment...)
	} else {
		cmd.Env = os.Environ()
	}

	if s.directory != "" {
		l.Trace().Str("dir", s.directory).Msg("setup directory")
		cmd.Dir = s.directory
	}

	if e := cmd.Run(); e != nil {
		err = fmt.Errorf("run command error: %w", e)
	}

	if ps := cmd.ProcessState; ps != nil {
		if !ps.Success() {
			err = errors.New("script failed")
		}
	}

	s.nextCheck = time.Now().Add(s.interval)
	return true, err
}

func (s *scriptCheck) MarshalZerologObject(e *zerolog.Event) {
	e.Str("name", "scriptCheck").
		Str("script", s.script).
		Strs("env", s.environment).
		Str("dir", s.directory).
		Dur("timeout", s.timeout).
		Dur("interval", s.interval).
		Time("nextCheck", s.nextCheck)
}

// TCPCheck monitor tcp port
type tcpCheck struct {
	address   string
	interval  time.Duration
	timeout   time.Duration
	nextCheck time.Time
}

func newTCPCheck(c *config.HCTCPConf) healthCheck {
	return &tcpCheck{
		address:   c.Address,
		nextCheck: time.Now(),
		interval:  intAsDuration(c.Interval, minInterval),
		timeout:   intAsDuration(c.Timeout, minTimeout),
	}
}

func (p *tcpCheck) check(ctx context.Context) (checked bool, err error) {
	if time.Now().Before(p.nextCheck) {
		return false, nil
	}

	l := zerolog.Ctx(ctx).With().Str("check", "tcp").Logger()
	l.Debug().Msg("healthcheck start")

	conn, err := net.DialTimeout("tcp", p.address, p.timeout)
	if err != nil {
		err = fmt.Errorf("tcp connection error: %w", err)
	}
	if conn != nil {
		conn.Close()
	}

	p.nextCheck = time.Now().Add(p.interval)
	return true, err
}

func (p *tcpCheck) MarshalZerologObject(e *zerolog.Event) {
	e.Str("name", "scriptCheck").
		Str("address", p.address).
		Dur("timeout", p.timeout).
		Dur("interval", p.interval).
		Time("nextCheck", p.nextCheck)
}

// httpCheck check is http responding
type httpCheck struct {
	url       string
	interval  time.Duration
	timeout   time.Duration
	nextCheck time.Time
}

func newHTTPCheck(c *config.HCHTTPConf) healthCheck {
	return &httpCheck{
		url:       c.URL,
		interval:  intAsDuration(c.Interval, minInterval),
		timeout:   intAsDuration(c.Timeout, minTimeout),
		nextCheck: time.Now(),
	}
}

func (p *httpCheck) check(ctx context.Context) (checked bool, err error) {
	if time.Now().Before(p.nextCheck) {
		return false, nil
	}

	l := zerolog.Ctx(ctx).With().Str("check", "http").Logger()
	l.Debug().Msg("healthcheck start")

	tr := &http.Transport{
		MaxIdleConns:    1,
		IdleConnTimeout: 1,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	client := &http.Client{Transport: tr, Timeout: p.timeout}
	resp, err := client.Get(p.url)

	switch {
	case err != nil:
		err = fmt.Errorf("http request error: %w", err)
	case resp != nil:
		l.Debug().Interface("response", resp).Msg("query response")
		if resp.StatusCode < 200 && resp.StatusCode >= 400 {
			err = fmt.Errorf("http request result: %d", resp.StatusCode)
		}

		resp.Body.Close()

	default:
		err = errors.New("http request: missing response")
	}

	p.nextCheck = time.Now().Add(p.interval)
	return true, err
}

func (p *httpCheck) MarshalZerologObject(e *zerolog.Event) {
	e.Str("name", "scriptCheck").
		Str("url", p.url).
		Dur("timeout", p.timeout).
		Dur("interval", p.interval).
		Time("nextCheck", p.nextCheck)
}

type healthChecker interface {
	check(ctx context.Context) (int, error)
}

type healthChecks []healthCheck

func (hc healthChecks) MarshalZerologArray(a *zerolog.Array) {
	for _, u := range hc {
		if m, ok := u.(zerolog.LogObjectMarshaler); ok {
			a.Object(m)
		} else {
			a.Interface(m)
		}
	}
}

// stdHealtChecker aggregate all checkers for one service
type stdHealtChecker struct {
	checks healthChecks
}

// newHealthChecker create new health checker
func newHealthChecker(ctx context.Context, conf *config.ServiceConf) (healthChecker, error) {
	var checks healthChecks
	for _, h := range conf.HealthChecks {
		switch c := h.(type) {
		case *config.HCNamedProcessConf:
			p, err := newProcessNameCheck(c)
			if err != nil {
				return nil, err
			}
			checks = append(checks, p)
		case *config.HCHTTPConf:
			checks = append(checks, newHTTPCheck(c))
		case *config.HCTCPConf:
			checks = append(checks, newTCPCheck(c))
		case *config.HCScriptConf:
			checks = append(checks, newScriptCheck(c))
		}
	}

	l := zerolog.Ctx(ctx)
	l.Debug().Array("checks", checks).Msg("newHealthChecker configured")

	if len(checks) == 0 {
		return &emptyHealthChecker{}, nil
	}

	lc := &stdHealtChecker{checks: checks}
	return lc, nil
}

// check run all checker and return actual number of check to first error and first error
func (h *stdHealtChecker) check(ctx context.Context) (int, error) {
	checked := 0
	for _, c := range h.checks {
		ok, err := c.check(ctx)
		if ok {
			checked++
		}
		if err != nil {
			return checked, err
		}
	}

	return checked, nil
}

// emptyHealthChecker always return 1 successful check
type emptyHealthChecker struct{}

// check run all checker and return actual number of check to first error and first error
func (h *emptyHealthChecker) check(ctx context.Context) (int, error) {
	return 1, nil
}
