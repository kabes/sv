package manager

//
// manager.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"strings"
	"sync"
	"time"

	ic "github.com/WAY29/icecream-go/icecream"
	"github.com/prometheus/client_golang/prometheus"

	"github.com/rs/zerolog"
	"github.com/samber/lo"

	"sv.app/internal/config"
	"sv.app/internal/logger"
	"sv.app/internal/os"
	"sv.app/internal/services"
	"sv.app/internal/support"
)

var _ = ic.Ic

// Manager keep information about all services and supervise it
type Manager struct {
	tasksQueue tasksQueue
	services   map[string]*serviceWrapper
	conf       *config.Config

	broker *support.Broker
	stopCh chan struct{}

	log zerolog.Logger

	confReloadUnix prometheus.Gauge

	depMgmr dependencyManager

	sync.RWMutex
}

// NewManager configure services according to configuration
func NewManager(conf *config.Config, broker *support.Broker) *Manager {
	log := logger.Logger.With().Str("subsystem", "manager").Logger()

	m := &Manager{
		broker:   broker,
		log:      log,
		services: make(map[string]*serviceWrapper),
		stopCh:   make(chan struct{}),
		confReloadUnix: prometheus.NewGauge(prometheus.GaugeOpts{
			Name: "sv_manager_config_load_unix",
			Help: "Unix time of last configuration load.",
		}),
	}
	prometheus.MustRegister(m.confReloadUnix)

	m.handleConfig(conf)

	return m
}

// Stop manager
func (m *Manager) Stop() error {
	m.log.Info().Msg("manager stopping")
	m.tasksQueue.clean()

	for _, s := range m.services {
		s.Close()
	}

	if m.stopCh != nil {
		m.stopCh <- struct{}{}
	}

	return nil
}

// Start manager
func (m *Manager) Start() error {
	m.log.Info().Msg("manager started")
	lsnr := m.broker.Subscribe()
	defer m.broker.Unsubscribe(lsnr)

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-m.stopCh:
			m.log.Info().Msg("manager stopped")
			m.stopCh = nil
			return nil

		case <-ticker.C:
			if m.tasksQueue.len() > 0 {
				m.handleManagerEvent(newManagerEvent(mEventNewAction))
			}
		case msg := <-lsnr:
			if d := m.log.Debug(); d.Enabled() {
				d.Interface("msg", msg).Str("type", fmt.Sprintf("%T", msg)).Msg("received msg")
			}
			switch msg := msg.(type) {
			case *config.Config:
				m.handleConfig(msg)
			case *services.InternalEvent:
				m.handleServiceInternalEvent(msg)
				m.handleManagerEvent(msg)
			case *event:
				m.handleManagerEvent(msg)
			case *services.StatusEvent:
				m.handleStatusEvent(msg)
			case Event:
				m.handleManagerEvent(msg)
			default:
				m.log.Warn().Interface("event", msg).Msg("not handled event")
			}
		}
	}
}

func (m *Manager) handleConfig(conf *config.Config) {
	m.Lock()
	defer m.Unlock()

	log := m.log
	log.Info().Interface("config", conf).Msg("new configuration received")

	existing := make(map[string]interface{}, len(conf.Services))

	created := 0
	updated := 0
	deleted := 0
	waitingDelete := 0

	for _, c := range conf.Services {
		existing[c.Name] = nil
		if s, ok := m.services[c.Name]; ok {
			// update existing
			if c.Changed(s.conf) {
				s.pendingConf = c
				log.Info().Str("service", c.Name).Msg("updating service configuration")
				updated++
				m.depMgmr.update(c.Name, c.Require)
			} else {
				log.Debug().Str("service", c.Name).Msg("skipping not changed configuration")
			}
		} else {
			log.Info().Str("service", c.Name).Msg("creating service")
			if srv, err := newServiceWrapper(m.broker, c); err == nil {
				m.services[c.Name] = srv
				m.depMgmr.update(c.Name, c.Require)
				created++
			} else {
				log.Error().Err(err).Str("service", c.Name).Msg("create new service error")
			}
		}
	}

	for n, s := range m.services {
		if _, ok := existing[n]; !ok {
			if s.status == services.StatusStopped {
				// stoped services may be deleted now
				s.Close()
				delete(m.services, n)
				log.Info().Str("service", n).Msg("service deleted")
				deleted++
				m.depMgmr.remove(n)
			} else {
				// but running services may be deleted after stop
				s.dispose = true
				log.Info().Str("service", n).Msg("service waiting to dispose")
				waitingDelete++
			}
		}
	}

	m.conf = conf
	m.confReloadUnix.SetToCurrentTime()

	log.Debug().
		Msgf("configuration updated: created=%d, updated=%d, deleted=%d, waiting_delete=%d",
			created, updated, deleted, waitingDelete)
	log.Debug().Msgf("depencencies: %s", m.depMgmr.String())
}

func (m *Manager) handleManagerEvent(event Event) {
	m.RLock()
	defer m.RUnlock()

	// process all tasks in queue
	for {
		task := m.tasksQueue.getNext(m)
		if task == nil {
			if l := m.log.Trace(); l.Enabled() {
				if queueLen := m.tasksQueue.len(); queueLen > 0 {
					l.Strs("queue", m.tasksQueue.status()).
						Strs("in_progress", m.getInProgressServices()).
						Func(m.logAddServicesStatus).
						Msg("task queue is not empty")
				}
			}
			return
		}

		args := task.Action
		l := m.log.With().Stringer("task_id", task.ID).Logger()
		l.Debug().
			Interface("task", task).
			Stringer("action", args.Action).
			Msg("processing task")

		var res string
		var err error

		switch args.Action {
		case ActionStart:
			res, err = m.handleStartTask(l.WithContext(context.Background()), args)
		case ActionStop:
			res, err = m.handleStopTask(l.WithContext(context.Background()), args)
		case ActionShutdown:
			m.Stop()
			return
		default:
			// other actions are not handled by tasks queue
			l.Warn().Stringer("action", args.Action).Msg("wrong action")
			continue
		}

		if err != nil {
			l.Error().Err(err).Msg("action error")
		} else if res != "" {
			l.Info().Interface("result", res).Msg("action ok")
		}

		l.Debug().Msg("processing task DONE")
	}
}

func (m *Manager) handleServiceInternalEvent(event *services.InternalEvent) {
	m.RLock()
	defer m.RUnlock()

	s, ok := m.services[event.Service]
	if !ok {
		m.log.Error().Str("service", event.Service).Msg("unknown service")
		return
	}

	s.AddEvent(event)

	switch event.Type {
	case services.EventHealtCheckFailed:
		fallthrough
	case services.EventProcessStopped:
		fallthrough
	case services.EventProcessFailedStart:
		fallthrough
	case services.EventProcessTerminated:
		if s.shouldRun && !s.dispose {
			// check & auto-restart
			m.log.Warn().Str("service", event.Service).Msg("service broken")
			s.ChangeStatus(services.StatusStopped, services.SubStatusBroken)
			if yes, delay := s.ShouldAutoRestart(); yes {
				var r []taskRunCondition
				if delay > 0 {
					r = append(r, newTimeCondition(delay))
				}
				m.log.Warn().Str("service", event.Service).Msgf("schedule service auto-restart with delay=%v", delay)
				m.tasksQueue.append(newTask(NewActionReq(s.Name, ActionStart).WithParam("mode", "auto-restart"), r))
			} else {
				s.shouldRun = false
			}
		} else {
			s.ChangeStatus(services.StatusStopped, services.SubStatusNone)
			s.shouldRun = false
		}
	case services.EventServiceFailedStop:
		s.ChangeStatus(services.StatusStopped, services.SubStatusBroken)
	case services.EventProcessRunning:
		s.ChangeStatus(services.StatusRunning, services.SubStatusNone)
	case services.EventServiceStarting:
		s.ChangeStatus(services.StatusStarting, services.SubStatusNone)
	case services.EventServiceStopping:
		s.ChangeStatus(services.StatusStopping, services.SubStatusNone)
	case services.EventServiceFailedReload:
		// ignored
	default:
		m.log.Error().Interface("event", event).Msg("not handled event")
		return
	}

	m.broker.Publish(services.NewStatusEvent(event.Service, s.status))
}

func (m *Manager) handleStatusEvent(event *services.StatusEvent) {
	srv := m.services[event.Service]
	srv.AddEvent(event)

	if event.Status == services.StatusStopped {
		m.Lock()
		defer m.Unlock()

		if srv.dispose {
			srv.Close()
			m.depMgmr.remove(srv.Name)
			delete(m.services, event.Service)
			m.log.Info().Str("service", event.Service).Msg("service deleted")
		}
	}

}

// ManagerShutdown manager and all services; usually force
func (m *Manager) ManagerShutdown(ctx context.Context) (string, error) {
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Warn().Msg("manager shutdown")

	m.tasksQueue.clean()

	m.tasksQueue.append(newTask(
		&ActionReq{Action: ActionShutdown, Services: nil},
		m.buildTaskConditions("", ActionShutdown),
	))

	m.tasksQueue.append(newTask(
		&ActionReq{
			Action:     ActionStop,
			Services:   m.getAllServicesNames(),
			Parameters: nil,
			isPack:     true,
		}, nil,
	))

	if d := l.Debug(); d.Enabled() {
		d.Interface("queue", m.tasksQueue.status()).Msg("queue")
	}

	m.broker.Publish(newManagerEvent(mEventNewAction))
	return "shutdown started", nil
}

// AutoStart call "autostart" action for all services
func (m *Manager) AutoStart(ctx context.Context) {
	m.RLock()
	defer m.RUnlock()

	services := support.FilterMapToSlice(m.services, func(_ string, s *serviceWrapper) (string, bool) {
		return s.Name, s.ShouldAutoStart()
	})

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()

	if len(services) == 0 {
		l.Info().Msg("autostart: no services")
		return
	}

	l.Info().Strs("service", services).Msg("autostart")

	var r []taskRunCondition
	if delay := m.conf.AutoStartDelay; delay > 0 {
		r = append(r, newTimeCondition(delay))
	}

	m.tasksQueue.append(newTask(
		&ActionReq{
			Action:   ActionStart,
			Services: services,
			isPack:   true,
		}, r))

	m.broker.Publish(newManagerEvent(mEventNewAction))
}

// Action perform given action on given services.
// This is generic entry for actions, in this time support stat & stop only.
// TODO: refactor
func (m *Manager) Action(ctx context.Context, args *ActionReq) (res []*ActionResult, err error) {
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("start action")

	if args.Action != ActionStart && args.Action != ActionStop {
		return nil, fmt.Errorf("invalid action: %v", args.Action)
	}

	services, err := m.validateActionServices(args)
	if err != nil {
		return nil, err
	}
	if err := m.checkServiceIsWaiting(services...); err != nil {
		return nil, err
	}

	noDep := args.IsParamSet("--no-dep") > 0

	switch {
	case noDep:
		// no dependency action
		l.Debug().Interface("args", args).Msg("no-dep")
	case args.Action == ActionStart:
		services, err = m.buildServicesToStartList(services...)
		l.Debug().Strs("services", services).Msg("start dependency")
	case args.Action == ActionStop:
		services, err = m.buildServicesToStopList(services...)
		l.Debug().Strs("services", services).Msg("stop dependency")
	}
	if err != nil {
		return nil, fmt.Errorf("build services tree failed: %v", err)
	}

	args.Services = services

	for _, aa := range args.Split() {
		s := aa.Services[0]
		var reqs []taskRunCondition
		if !noDep {
			reqs = m.buildTaskConditions(s, aa.Action)
		}
		l.Debug().Str("service", s).Interface("req", reqs).Msgf("requirements, noDep=%v", noDep)
		m.tasksQueue.append(newTask(aa, reqs))
		res = append(res, NewActionResultOK(s, "scheduled "+string(args.Action)+" "+s))
	}

	m.broker.Publish(newManagerEvent(mEventNewAction))

	return res, nil
}

// ManagerStatus return manager status (queue, services status) - internal, not user action
func (m *Manager) ManagerStatus(ctx context.Context) (*Status, error) {
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Msg("start manager status action")

	statuses := lo.MapEntries(m.services, func(n string, s *serviceWrapper) (string, string) {
		return n, string(s.Status())
	})

	ms := Status{
		Queue:         m.tasksQueue.status(),
		ServiceStatus: statuses,
	}
	return &ms, nil
}

// ManagerAbort clean current tasks queue
func (m *Manager) ManagerAbort(ctx context.Context) (string, error) {
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Msg("start manager abort action")

	removed := m.tasksQueue.clean()
	return fmt.Sprintf("removed %d task from queue", removed), nil
}

// CollectMetrics collect metrics from services
func (m *Manager) CollectMetrics(cb func(m *Metrics)) {
	m.RLock()
	defer m.RUnlock()

	for _, s := range m.services {
		cb(s.Metrics())
	}

}

// validateActionServices verify list of services in action argument.
// If action require direct one of more service and action don't have any - error is returned.
// If given list of services is empty or list contain 'all' - returned is list of all services.
// Otherwise checked is list of given services and if any service is invalid - error is returned.
func (m *Manager) validateActionServices(args *ActionReq) ([]string, error) {
	if args.Action.RequreService() && len(args.Services) == 0 {
		return nil, fmt.Errorf("action require service")
	}

	if args.AllServices() {
		return m.getAllServicesNames(), nil
	}

	if updated, s := m.replaceGroups(args.Services); updated {
		m.log.Debug().Strs("services", s).Msg("group replaced")
		args.Services = s
	}

	missingServices := lo.Filter(args.Services, func(s string, _ int) bool {
		_, ok := m.services[s]
		return !ok
	})

	if len(missingServices) > 0 {
		return nil, fmt.Errorf("unknown services: %s", strings.Join(missingServices, ", "))
	}

	if len(args.Services) > 1 {
		return lo.Uniq(args.Services), nil
	}

	return args.Services, nil
}

func (m *Manager) checkServiceIsWaiting(service ...string) error {
	waiting := m.tasksQueue.servicesWaiting(service...)
	if len(waiting) > 0 {
		return fmt.Errorf("services waiting for action: %s", strings.Join(waiting, ", "))
	}

	return nil
}

// actionGeneric run cb function for all given in arts services.
// If cb return nil - result is ignored
func (m *Manager) actionGeneric(services []string,
	cb func(*serviceWrapper, int) *ActionResult) (res []*ActionResult, err error) {

	s, err := m.getServices(services...)
	if err != nil {
		return nil, err
	}
	if len(s) == 0 {
		return nil, errors.New("no service")
	}

	res = lo.Map(s, cb)
	res = lo.Without(res, nil)

	return res, nil

}

// ActionReloadLogs reload logs for all or selected services
func (m *Manager) ActionReloadLogs(ctx context.Context, args *ActionReq) (res []*ActionResult, err error) {
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("start action")

	services, err := m.validateActionServices(args)
	if err != nil {
		return nil, err
	}

	return m.actionGeneric(services,
		func(srv *serviceWrapper, _ int) *ActionResult {
			res, err := srv.ActionReloadLogs(ctx)
			if err != nil {
				l.Error().Err(err).Str("service", srv.Name).Msg("reload log error")
			}
			return NewActionResult(srv.Name, err, res)
		})
}

// ActionEvents get events for specified services
func (m *Manager) ActionEvents(ctx context.Context, args *ActionReq) (res []*EventInfo, err error) {
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("action events")

	if args.Services, err = m.validateActionServices(args); err != nil {
		return nil, err
	}

	services, err := m.getServices(args.Services...)
	if err != nil {
		return nil, err
	}

	for _, srv := range services {
		res = append(res, srv.ActionEvents(ctx)...)
	}

	if len(args.Services) > 1 {
		sort.Slice(res, func(i1, i2 int) bool {
			return res[i1].TS.Before(res[i2].TS)
		})
	}

	return
}

// ActionSignal send given signal to service or services
func (m *Manager) ActionSignal(ctx context.Context, args *ActionReq) (res []*ActionResult, err error) {
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("start action")

	services, err := m.validateActionServices(args)
	if err != nil {
		return nil, err
	}

	if args.Parameters == nil {
		return nil, errors.New("require parameters")
	}
	signame := args.Parameters["--signal"]
	if _, ok := os.GetSignal(signame); !ok {
		return nil, fmt.Errorf("missing or wrong signal name")
	}

	return m.actionGeneric(services,
		func(srv *serviceWrapper, _ int) *ActionResult {
			res, err := srv.ActionSignal(ctx, signame)
			if err != nil {
				l.Error().Err(err).Str("service", srv.Name).Msg("signal service error")
			}
			return NewActionResult(srv.Name, err, res)
		})
}

// ActionReload service (send signal, call reload script)
func (m *Manager) ActionReload(ctx context.Context, args *ActionReq) (res []*ActionResult, err error) {
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("start action")

	services, err := m.validateActionServices(args)
	if err != nil {
		return nil, err
	}
	if err := m.checkServiceIsWaiting(services...); err != nil {
		return nil, err
	}

	return m.actionGeneric(services,
		func(srv *serviceWrapper, _ int) *ActionResult {
			err := srv.ActionReload(ctx)
			if err != nil {
				l.Error().Err(err).Str("service", srv.Name).Msg("reload service error")
			}
			return NewActionResult(srv.Name, err, "reloaded")
		})
}

// ActionStatus get status of services and manager
func (m *Manager) ActionStatus(ctx context.Context, args *ActionReq) (statuses []*services.StatusInfo, queue []string, err error) {
	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("action status")

	s, err := m.validateActionServices(args)
	if err != nil {
		return nil, nil, err
	}

	verbose := args.IsParamSet("--verbose", "-v")
	statuses = lo.Map(s, func(service string, _ int) *services.StatusInfo {
		srv := m.services[service]
		r := srv.ActionStatus(ctx, verbose)
		l.Debug().Interface("result", r).Msg("action result")
		return r
	})

	queue = m.tasksQueue.status()
	return
}

// ActionReadLogs read last log lines
func (m *Manager) ActionReadLogs(ctx context.Context, args *ActionReq, buf *[]byte) (int64, error) {
	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("action read logs")

	snames, err := m.validateActionServices(args)
	if err != nil {
		return 0, err
	}
	if len(snames) != 1 {
		return 0, errors.New("one and only one service required")
	}

	services, err := m.getServices(snames[0])
	if err != nil {
		return 0, err
	}

	srv := services[0]
	offset, err := args.ParamAsInt64("offset", 0)
	if err != nil {
		return 0, fmt.Errorf("invalid parameter `offset`: %w", err)
	}

	output := args.Parameters["--output"]
	if !lo.Contains([]string{"", "err", "out", "stdout", "stderr"}, output) {
		return 0, fmt.Errorf("invalid 'output' parameter; required 'err', 'out', 'stdout' (default) or 'stderr'")
	}

	offset, err = srv.ActionLogs(l.WithContext(ctx), output, offset, buf)
	return offset, err
}

func (m *Manager) handleStartTask(ctx context.Context, args *ActionReq) (res string, err error) {
	l := zerolog.Ctx(ctx)

	// if task is bundle, split it
	if !args.IsSingleton() {
		// split action

		if args.AllServices() {
			args.Services = m.getAllServicesNames()
		}

		services, err := m.buildServicesToStartList(args.Services...)
		if err != nil {
			return "", fmt.Errorf("build services tree failed: %v", err)
		}
		args.Services = services

		for _, aa := range args.Split() {
			s := aa.Services[0]
			reqs := m.buildTaskConditions(s, aa.Action)
			l.Debug().Str("service", s).Interface("req", reqs).Msg("requirements")
			t := newTask(aa, reqs)
			l.Debug().Interface("task", t).Msg("new task created")
			m.tasksQueue.append(t)
		}

		return "scheduled " + string(args.Action), nil
	}

	// single task
	srvName := args.Services[0]
	srv, ok := m.services[srvName]
	if !ok {
		return "", fmt.Errorf("service '%s' not found", srvName)
	}

	// TODO: context
	return srv.ActionStart(ctx)
}

// ActionPID get pid of services process
func (m *Manager) ActionPID(ctx context.Context, args *ActionReq) (res []*ActionResult, err error) {
	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("start PID action")

	services, err := m.validateActionServices(args)
	if err != nil {
		return nil, err
	}

	return m.actionGeneric(services,
		func(srv *serviceWrapper, _ int) *ActionResult {
			pid, err := srv.ActionPID(ctx)
			return NewActionResult(srv.Name, err, pid)
		})
}

func (m *Manager) handleStopTask(ctx context.Context, args *ActionReq) (res string, err error) {
	l := zerolog.Ctx(ctx)

	if !args.IsSingleton() {
		// split action
		if args.AllServices() {
			args.Services = m.getAllServicesNames()
		}

		for _, aa := range args.Split() {
			s := aa.Services[0]
			reqs := m.buildTaskConditions(s, aa.Action)
			l.Debug().Str("service", s).Msgf("requirements: %v", reqs)
			t := newTask(aa, reqs)
			l.Debug().Interface("task", t).Msg("new task created")
			m.tasksQueue.append(t)
		}

		return "scheduled " + string(args.Action), nil
	}

	srvName := args.Services[0]
	srv, ok := m.services[srvName]
	if !ok {
		return "", fmt.Errorf("wrong service name %s", srvName)
	}

	return srv.ActionStop(ctx)
}

// ActionRestart restart service or services
// TODO: verify start
func (m *Manager) ActionRestart(ctx context.Context, args *ActionReq) (res []*ActionResult, err error) {
	// restart is special action; all services are stopped according to dependencies
	// and on task to run all tasks and is depended to all services stopped is scheduled
	m.RLock()
	defer m.RUnlock()

	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("start action")

	args.Services, err = m.validateActionServices(args)
	if err != nil {
		return nil, err
	}
	if err := m.checkServiceIsWaiting(args.Services...); err != nil {
		return nil, err
	}

	noDep := args.IsParamSet("--no-dep") > 0

	// all services to stop and rerun. If no-dep parameter is not set restart also all dependents
	s := args.Services
	if !noDep {
		s, _ = m.buildServicesToStopList(args.Services...)
	}

	// generate start task for all stopped services
	req := lo.Map(s, func(s string, _ int) taskRunCondition {
		return newServiceStatusCondition(s, services.StatusStopped)
	})
	m.tasksQueue.append(newTask(&ActionReq{
		Action:     ActionStart,
		Services:   s[:],
		Parameters: args.Parameters,
		isPack:     true,
	}, req))

	res = append(res, NewActionResultOK("MANAGER", fmt.Sprintf("scheduled start %s", s)))

	// generate stop task
	args.Action = ActionStop
	args.Services = s

	for _, aa := range args.Split() {
		s := aa.Services[0]
		var reqs []taskRunCondition
		if !noDep {
			reqs = m.buildTaskConditions(s, aa.Action)
		}
		l.Debug().Str("service", s).Interface("req", reqs).Msg("requirements")
		m.tasksQueue.append(newTask(aa, reqs))
		res = append(res, NewActionResultOK(s, "scheduled stop "+s))
	}

	m.broker.Publish(newManagerEvent(mEventNewAction))

	return
}

// ManagerReload reload configuration
func (m *Manager) ManagerReload(ctx context.Context, args *ActionReq) (string, error) {
	l := zerolog.Ctx(ctx).With().Str("subsystem", "manager").Logger()
	l.Info().Interface("args", args).Msg("start manager reload action")

	conf, err := config.Reload(m.conf, "")
	if err != nil {
		l.Error().Err(err).Msg("load configuration error")
		return "", fmt.Errorf("error load configuration: %w", err)
	}

	if conf != nil {
		m.broker.Publish(conf)
		return "configuration loaded", nil
	}

	return "configuration not changed", nil
}

func (m *Manager) getServices(names ...string) ([]*serviceWrapper, error) {
	var services []*serviceWrapper
	var errors []string

	lo.ForEach(names, func(as string, _ int) {
		if s, ok := m.services[as]; ok {
			services = append(services, s)
		} else {
			errors = append(errors, as)
		}
	})

	if len(errors) > 0 {
		return services, fmt.Errorf("unknown services: %s", strings.Join(errors, ", "))
	}

	return services, nil
}

func (m *Manager) getAllServicesNames() (names []string) {
	return lo.Keys(m.services)
}

// buildTaskConditions create list of conditions required for perform `action` on service
func (m *Manager) buildTaskConditions(service string, action Action) (tc []taskRunCondition) {
	switch action {
	case ActionStart:
		// direct required service
		for _, r := range m.depMgmr.serviceDependency(service) {
			tc = append(tc, newServiceStatusCondition(r, services.StatusRunning))
		}
	case ActionStop:
		// look into all services
		for _, s := range m.depMgmr.serviceDependant(service) {
			tc = append(tc, newServiceStatusCondition(s, services.StatusStopped))
		}
	case ActionShutdown:
		for _, s := range m.services {
			tc = append(tc, newServiceStatusCondition(s.Name, services.StatusStopped))
		}
	}

	return
}

func (m *Manager) buildServicesToStartList(service ...string) ([]string, error) {
	deps := m.depMgmr.serviceDependency(service...)
	deps = append(deps, service...)
	res := lo.Filter(deps, func(s string, i int) bool {
		srv, ok := m.services[s]
		return ok && srv.status == services.StatusStopped
	})
	return res, nil
}

func (m *Manager) buildServicesToStopList(service ...string) ([]string, error) {
	deps := m.depMgmr.serviceDependant(service...)
	deps = append(deps, service...)
	res := lo.Filter(deps, func(s string, i int) bool {
		srv, ok := m.services[s]
		return ok && srv.status == services.StatusRunning
	})
	return res, nil
}

func (m *Manager) getInProgressServices() (srvs []string) {
	return support.FilterMapToSlice(m.services, func(k string, s *serviceWrapper) (string, bool) {
		return k, s.status.InProgress()
	})
}

// replaceGroups find group:xx in service list and replace it with proper service names.
// 'group:' is replaced by services without group.
func (m *Manager) replaceGroups(services []string) (bool, []string) {
	res := make([]string, 0, len(services))
	updated := false
	for _, s := range services {
		if !strings.HasPrefix(s, "group:") {
			res = append(res, s)
			continue
		}

		s = s[6:]
		for n, srv := range m.services {
			if (s != "" && lo.Contains(srv.conf.Groups, s)) ||
				(s == "" && len(srv.conf.Groups) == 0) {
				res = append(res, n)
				updated = true
			}
		}
	}

	if updated {
		res = lo.Uniq(res)
	}

	return updated, res
}

func (m *Manager) logAddServicesStatus(e *zerolog.Event) {
	for _, s := range m.services {
		e.Str("service_"+s.Name, string(s.status))
	}
}
