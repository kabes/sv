package manager

//
// service.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/rs/zerolog"

	//	ic "github.com/WAY29/icecream-go/icecream"

	"sv.app/internal/config"
	"sv.app/internal/logger"
	"sv.app/internal/services"
	"sv.app/internal/support"
)

//----------------------

// serviceWrapper keep all information about running service
type serviceWrapper struct {
	Name        string
	shouldRun   bool
	status      services.Status
	subStatus   services.SubStatus
	startRetry  uint
	totalStarts uint
	totalErrors uint

	// startTime is timestamp of execute start action, not confirmed running
	startTime time.Time
	stopTime  time.Time
	// lastSuccHC is timestamp of last successful health check
	lastSuccHC time.Time

	conf *config.ServiceConf
	impl services.ServiceImpl
	// pendingConf is new configuration that will be apply on next start
	pendingConf *config.ServiceConf
	// dispose means that service not exists anymore in config and should be removed
	dispose bool

	hcTotalErrors uint

	broker *support.Broker
	events eventLogger

	// cancelHc stop healthchecker
	cancelHc func()

	log zerolog.Logger
	sync.RWMutex
}

// NewService create new service according to configuration
func newServiceWrapper(broker *support.Broker, conf *config.ServiceConf) (*serviceWrapper, error) {
	impl, err := services.NewService(conf, broker)
	if err != nil {
		return nil, err
	}

	srv := &serviceWrapper{
		Name:      conf.Name,
		conf:      conf,
		impl:      impl,
		broker:    broker,
		events:    newEventLogger(),
		status:    services.StatusStopped,
		subStatus: services.SubStatusNone,
		log:       logger.Logger.With().Str("service", conf.Name).Logger(),
	}

	return srv, nil
}

// Close service
func (s *serviceWrapper) Close() {
}

// AddEvent add new event to history
func (s *serviceWrapper) AddEvent(e Event) {
	s.Lock()
	defer s.Unlock()

	s.events.Add(e)
}

// Status return current service status
func (s *serviceWrapper) Status() services.Status {
	s.RLock()
	defer s.RUnlock()

	return s.status
}

// Metrics return metrics for service
func (s *serviceWrapper) Metrics() *Metrics {
	s.RLock()
	defer s.RUnlock()

	startTS := int64(0)
	// set start ts only when service is running
	if !s.startTime.IsZero() && s.stopTime.IsZero() {
		startTS = s.startTime.Unix()
	}

	return &Metrics{
		Name:          s.Name,
		Status:        string(s.status),
		StartTS:       startTS,
		Broken:        s.subStatus == services.SubStatusBroken,
		Starts:        s.totalStarts,
		TotalErrors:   s.totalErrors,
		HCTotalErrors: s.hcTotalErrors,
		ConfPending:   s.pendingConf != nil,
	}
}

// ChangeStatus change service status
func (s *serviceWrapper) ChangeStatus(status services.Status, subStatus services.SubStatus) {
	s.Lock()
	defer s.Unlock()

	if status == s.status && subStatus == s.subStatus {
		return
	}

	s.log.Info().Str("status", string(status)).
		Str("substatus", string(subStatus)).
		Msg("service changed status")

	s.status = status
	s.subStatus = subStatus

	switch status {
	case services.StatusStopped:
		if s.cancelHc != nil {
			s.cancelHc()
			s.cancelHc = nil
		}
		if s.stopTime.IsZero() {
			s.stopTime = time.Now()
		}
	case services.StatusRunning:
	case services.StatusStarting:
		if err := s.startHealtChecker(); err != nil {
			s.log.Error().Err(err).Msg("start healtchecker error")
		}
	}

	if subStatus == services.SubStatusBroken {
		s.totalErrors++
	}
}

// ActionStatus return status of service
func (s *serviceWrapper) ActionStatus(ctx context.Context, verbose int) *services.StatusInfo {
	s.RLock()
	defer s.RUnlock()

	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})
	l.Trace().Msg("action status - service")

	// get status from implementation
	res := s.impl.Status(l.WithContext(ctx), verbose)
	if res == nil {
		res = services.NewServiceStatus(s.Name)
	}

	res.Status = s.status
	res.SubStatus = s.subStatus

	// add additional data when verbose
	if verbose > 0 {
		res.AddDate("start_time", s.startTime)
		res.AddDate("stop_time", s.stopTime)
		res.AddDate("last_ok", s.lastSuccHC)
		if s.dispose {
			res.AddWarning("service will be deleted")
		} else if s.pendingConf != nil {
			res.AddWarning("service configuration changed")
		}
	}

	return res
}

// ActionStop stop service
func (s *serviceWrapper) ActionStop(ctx context.Context) (string, error) {
	s.Lock()
	defer s.Unlock()

	if s.status != services.StatusRunning {
		return "", fmt.Errorf("cannot stop service; is %s", s.status)
	}

	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})

	s.shouldRun = false
	s.broker.Publish(services.NewInternalEvent(s.Name, services.EventServiceStopping))

	if err := s.impl.Stop(l.WithContext(ctx)); err != nil {
		s.events.Add(services.NewInternalEvent(s.Name, services.EventServiceFailedStop,
			"stop error: "+err.Error()))
		return "", fmt.Errorf("stop error: %w", err)
	}

	return "stopping", nil
}

// ActionSignal signal service
func (s *serviceWrapper) ActionSignal(ctx context.Context, signame string) (string, error) {
	s.Lock()
	defer s.Unlock()

	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})

	if s.status != services.StatusRunning {
		return "service is not running", nil
	}

	s.broker.Publish(services.NewInternalEvent(s.Name, services.EventGeneric, "signaling "+signame))

	if i, ok := s.impl.(services.ServiceImplProcess); ok {
		if err := i.Signal(l.WithContext(ctx), signame); err != nil {
			return "", err
		}

		return "signaled", nil
	}

	return "", errors.New("not supported")
}

// ActionReloadLogs reload service log files
func (s *serviceWrapper) ActionReloadLogs(ctx context.Context) (string, error) {
	s.Lock()
	defer s.Unlock()

	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})

	if s.status == services.StatusStopped {
		return "service is stopped", nil
	}

	if err := s.impl.ReloadLogger(l.WithContext(ctx)); err != nil {
		return "", fmt.Errorf("reload logs error: %w", err)
	}

	s.events.Add(services.NewInternalEvent(s.Name, services.EventServiceLogReload))
	return "reloaded", nil
}

// ActionStart start service
func (s *serviceWrapper) ActionStart(ctx context.Context) (string, error) {
	s.Lock()
	defer s.Unlock()

	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})

	l.Debug().Msg("actionStart")

	if s.status != services.StatusStopped {
		return "", fmt.Errorf("cannot start service; is %s", s.status)
	}

	if s.pendingConf != nil {
		l.Info().Msg("apply new configuration")
		s.conf = s.pendingConf
		s.pendingConf = nil
	}

	s.shouldRun = true
	s.totalStarts++
	s.startTime = time.Now()
	s.stopTime = time.Time{}

	if err := s.impl.Start(l.WithContext(ctx)); err != nil {
		s.events.Add(services.NewInternalEvent(s.Name, services.EventProcessFailedStart,
			"start error: "+err.Error()))
		return "", fmt.Errorf("start error: %w", err)
	}

	return "starting", nil
}

// ActionEvents get events from service
func (s *serviceWrapper) ActionEvents(ctx context.Context) []*EventInfo {
	s.RLock()
	defer s.RUnlock()

	return s.events.Get()
}

// ActionLogs get last logs for service.
// Read max len(buf) from offset into buf and return new offset or error
func (s *serviceWrapper) ActionLogs(ctx context.Context, output string, offset int64, buf *[]byte) (int64, error) {
	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})

	return s.impl.Logs(l.WithContext(ctx), output, offset, buf)
}

// ActionPID of running service process
func (s *serviceWrapper) ActionPID(ctx context.Context) (int, error) {
	s.RLock()
	defer s.RUnlock()

	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})

	if i, ok := s.impl.(services.ServiceImplProcess); ok {
		return i.Pid(l.WithContext(ctx))
	}

	return 0, errors.New("not supported")
}

// ActionReload of reloading service process
func (s *serviceWrapper) ActionReload(ctx context.Context) error {
	s.RLock()
	defer s.RUnlock()

	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})

	if s.status != services.StatusRunning {
		return fmt.Errorf("service not running")
	}

	err := s.impl.Reload(l.WithContext(ctx))
	if err != nil {
		s.events.Add(services.NewInternalEvent(s.Name, services.EventServiceFailedReload,
			"reload failed: "+err.Error()))
	}

	return err
}

// Check internally is service is running
func (s *serviceWrapper) Check(ctx context.Context) error {
	l := zerolog.Ctx(ctx)
	l.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("service", s.Name)
	})

	return s.impl.Check(l.WithContext(ctx))
}

// ShouldAutoRestart check is service can be automatically restarted
// return (do_restart, delay)
func (s *serviceWrapper) ShouldAutoRestart() (bool, uint) {
	return s.impl.ShouldAutoRestart(s.startRetry)
}

// ShouldAutoStart check is service should be automatically started
func (s *serviceWrapper) ShouldAutoStart() bool {
	return s.impl.ShouldAutoStart()
}

func (s *serviceWrapper) startHealtChecker() error {
	l := s.log.With().Str("subsystem", "hc").Logger()

	if s.cancelHc != nil {
		l.Warn().Msg("start when previous hc running; closing")
		s.cancelHc()
		s.cancelHc = nil
	}

	ctx := l.WithContext(context.Background())

	hc, err := newHealthChecker(ctx, s.conf)
	if err != nil {
		return fmt.Errorf("prepare healthchecker error: %w", err)
	}

	ctx, s.cancelHc = context.WithCancel(ctx)

	go func() {
		defer func() {
			if s.cancelHc != nil {
				s.cancelHc()
			}
		}()

		ticker := time.NewTicker(1 * time.Second)
		defer ticker.Stop()

		waitReady := float64(s.conf.StartTimeout)

		l.Info().Msg("healthcheck started")

	breakfor:
		for {
			// wait 1sec to next check
			select {
			case <-ctx.Done():
				l.Info().Msg("healthcheck stopped (done)")
				break breakfor
			case <-ticker.C:
			}

			if s.status == services.StatusStopped {
				l.Debug().Msg("service stopped; finish health checker")
				break breakfor
			}

			// internal check
			err := s.Check(ctx)
			if err == nil {
				// Run other checks. When no checks are defined - emptyHealthChecker
				// is used so always "internal" check decide
				var checked int
				if checked, err = hc.check(ctx); checked == 0 {
					// when no check are performed - do no update any status
					continue
				}
			}
			if err == nil {
				s.lastSuccHC = time.Now()
			} else {
				s.hcTotalErrors++
			}

			switch s.status {
			case services.StatusStopping:
				if err != nil {
					// its ok to ignore errors; service is stopping so we expect it
					l.Debug().Err(err).Msg("service stopping, check failed - ok")
					s.broker.Publish(services.NewInternalEvent(s.Name, services.EventProcessStopped))
					break breakfor
				}

			case services.StatusStarting:
				// wait to service start
				if err == nil {
					l.Debug().Msg("service is now running")
					s.broker.Publish(services.NewInternalEvent(s.Name, services.EventProcessRunning))
					break
				}

				// do not fail if service is still in waitReady time
				if time.Since(s.startTime).Seconds() > waitReady {
					l.Debug().Err(err).Msg("service not started in `waitReady` time")
					s.broker.Publish(services.NewInternalEvent(s.Name, services.EventProcessFailedStart))
					// FIXME: break?
				}

			case services.StatusRunning:
				if err != nil {
					l.Info().Err(err).Msg("healthcheck failed")
					s.broker.Publish(services.NewInternalEvent(s.Name, services.EventHealtCheckFailed, err.Error()))
					break breakfor
				}

				if s.startRetry > 0 && time.Since(s.startTime).Seconds() > waitReady {
					// reset restart counter after some time
					s.startRetry = 0
				}
			}
		}

		l.Info().Msg("healthcheck stopped")
	}()

	return nil
}

func (s *serviceWrapper) Dependencies() []string {
	return s.conf.Require[:]
}

func (s *serviceWrapper) DependOn(service string) bool {
	return s.conf.DependOn(service)
}

type serviceDependencyInfo interface {
	Dependencies() []string
	DependOn(service string) bool
}
