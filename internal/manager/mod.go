package manager

import (
	"fmt"
	"strings"
	"time"
	//	. "github.com/WAY29/icecream-go/icecream"
)

// Status is global status of Manager & services
type Status struct {
	TS            time.Time
	ServiceStatus map[string]string
	Queue         []string
}

// Lines get formatted content
func (m *Status) Lines() []string {
	res := make([]string, 0, len(m.ServiceStatus)+1)
	ts := m.TS.Format(time.RFC3339)
	for s, v := range m.ServiceStatus {
		res = append(res, fmt.Sprintf("%s\t%s:\t%s", ts, s, v))
	}

	res = append(res, fmt.Sprintf("%s\tQueue:\t%s", ts, strings.Join(m.Queue, ", ")))
	return res
}

// Metrics keep metrics for service
type Metrics struct {
	Name          string
	Status        string
	StartTS       int64
	Broken        bool
	Starts        uint
	TotalErrors   uint
	HCTotalErrors uint
	ConfPending   bool
}
