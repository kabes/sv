package manager

//
// actions.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"fmt"
	"strconv"
	"strings"

	"sv.app/internal/support"
)

// Action define available actions
type Action string

const (
	// ActionStart = start service or services
	ActionStart Action = "start"
	// ActionStop = stop service or services
	ActionStop Action = "stop"
	// ActionRestart = restart service or services
	ActionRestart Action = "restart"
	// ActionSignal send signal to service; argument required
	ActionSignal Action = "signal"
	// ActionStatus return status of service/services
	ActionStatus Action = "status"
	// ActionReload = ie reload configuration
	ActionReload Action = "reload"

	// ActionReloadConf = ie reload configuration
	ActionReloadConf Action = "reload-conf"

	// ActionEvents = get last 10 events
	ActionEvents Action = "events"

	// ActionReloadLogs close and open log files
	ActionReloadLogs = "reload-logs"

	// ActionAbort clear waiting tasks queue
	ActionAbort = "abort"

	// ActionShutdown stop all services and shutdown sv
	ActionShutdown = "shutdown"

	// ActionPID get process pid for service or services
	ActionPID = "pid"
)

// RequreService return true when given action require service as parameter
func (a Action) RequreService() bool {
	return (a == ActionStart ||
		a == ActionStop ||
		a == ActionRestart ||
		a == ActionSignal ||
		a == ActionReload)
}

func (a Action) String() string {
	return string(a)
}

// ActionReq keep all arguments for action
type ActionReq struct {
	Action     Action
	Services   []string
	Parameters map[string]string
	isPack     bool
}

// NewActionReq create new action for given services
func NewActionReq(service string, action Action) *ActionReq {
	return &ActionReq{
		Action:     action,
		Services:   []string{service},
		Parameters: nil,
	}
}

// WithParam add parameter to ActionReq
func (aa *ActionReq) WithParam(key, val string) *ActionReq {
	if aa.Parameters == nil {
		aa.Parameters = make(map[string]string)
	}
	aa.Parameters[key] = val
	return aa
}

func (aa *ActionReq) String() string {
	return fmt.Sprintf("%s %s", aa.Action, strings.Join(aa.Services, ","))
}

// Equal return true when two ActionReq are equal
func (aa *ActionReq) Equal(a *ActionReq) bool {
	return a.Action == aa.Action &&
		support.CompareSliceShallow(a.Services, aa.Services) &&
		support.CompareMaps(a.Parameters, aa.Parameters) &&
		a.isPack == aa.isPack
}

// AllServices check is command apply to all services
func (aa *ActionReq) AllServices() bool {
	if len(aa.Services) == 0 {
		return true
	}
	for _, s := range aa.Services {
		if s == "all" {
			return true
		}
	}

	return false
}

// ContainService return true when service is in the action.
func (aa *ActionReq) ContainService(service ...string) bool {
	for _, srv := range aa.Services {
		for _, s := range service {
			if srv == s {
				return true
			}
		}
	}

	return false
}

// IsSingleton check is command apply only to one service
func (aa *ActionReq) IsSingleton() bool {
	return len(aa.Services) == 1 && aa.Services[0] != "all" && !aa.isPack
}

// Split create copy ActionArgument for each service
func (aa *ActionReq) Split() (res []*ActionReq) {
	for _, s := range aa.Services {
		res = append(res, &ActionReq{
			Action:     aa.Action,
			Services:   []string{s},
			Parameters: aa.Parameters,
			isPack:     false,
		})
	}
	return
}

// ParamAsInt get parameter as int value; return default value if not exists.
func (aa *ActionReq) ParamAsInt(name string, defaultValue int) (int, error) {
	if aa.Parameters == nil {
		return defaultValue, nil
	}
	strval, ok := aa.Parameters[name]
	if !ok {
		return defaultValue, nil
	}
	return strconv.Atoi(strval)
}

// ParamAsInt64 get parameter as int value; return default value if not exists.
func (aa *ActionReq) ParamAsInt64(name string, defaultValue int64) (int64, error) {
	if aa.Parameters == nil {
		return defaultValue, nil
	}
	strval, ok := aa.Parameters[name]
	if !ok {
		return defaultValue, nil
	}
	return strconv.ParseInt(strval, 10, 64)
}

// IsParamSet get number of set parameters by name
func (aa *ActionReq) IsParamSet(names ...string) int {
	if aa.Parameters == nil {
		return 0
	}

	res := 0
	for _, n := range names {
		if _, ok := aa.Parameters[n]; ok {
			res++
		}
	}

	return res
}

// ActionResult is generic result returned by actions operating on services
type ActionResult struct {
	Service string
	Error   string
	Data    interface{}
}

// NewActionResult create simple ActionResult from err or data
// When err != nil, action result is Error
// When err == nil and no data given - result is Noop
// Otherwise result is OK
func NewActionResult(service string, err error, data interface{}) *ActionResult {
	if err != nil {
		return &ActionResult{
			Service: service,
			Error:   err.Error(),
		}
	}

	return &ActionResult{
		Service: service,
		Data:    data,
	}
}

// NewActionResultOK create new ActionResult with success
func NewActionResultOK(object string, data interface{}) *ActionResult {
	return &ActionResult{
		Service: object,
		Data:    data,
	}
}
