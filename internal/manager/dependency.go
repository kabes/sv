package manager

import (
	"fmt"
	"strings"

	"github.com/samber/lo"
	"sv.app/internal/config"
	"sv.app/internal/support"
)

//
// dependency.go
// Copyright (C) 2023 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

type serviceDependency struct {
	name     string
	dependOn []string
}

type dependencyManager struct {
	services []*serviceDependency
}

func (d *dependencyManager) load(conf *config.Config) {
	d.services = make([]*serviceDependency, 0, len(conf.Services))
	for name, c := range conf.Services {
		d.services = append(d.services, &serviceDependency{
			name: name, dependOn: c.Require,
		})
	}
}

func (d *dependencyManager) update(service string, dependOn []string) {
	if s := d.find(service); s != nil {
		s.dependOn = dependOn
		return
	}

	d.services = append(d.services, &serviceDependency{
		name: service, dependOn: dependOn,
	})
}

func (d *dependencyManager) remove(service string) {
	for i, s := range d.services {
		if s.name == service {
			d.services = support.Remove(d.services, i)
			return
		}
	}
}

func (d *dependencyManager) findServicesDependOn(service string) []string {
	var res []string
	for _, s := range d.services {
		if lo.Contains(s.dependOn, service) {
			res = append(res, s.name)
		}
	}
	return res
}

// find serviceDependency by service name
func (d *dependencyManager) find(service string) *serviceDependency {
	for _, s := range d.services {
		if s.name == service {
			return s
		}
	}

	return nil
}

// serviceDependency build list of services that are required by given `services`.
func (d *dependencyManager) serviceDependency(service ...string) []string {
	deps := make(map[string]struct{}, len(d.services)*2)
	for _, s := range service {
		deps[s] = struct{}{}
	}
	pService := service[:]
	for len(pService) > 0 {
		var newServices []string
		for _, s := range pService {
			deps[s] = struct{}{}
			srv := d.find(s)
			if srv == nil {
				return nil
			}
			for _, r := range srv.dependOn {
				if _, ok := deps[r]; !ok {
					deps[r] = struct{}{}
					newServices = append(newServices, r)
				}
			}
		}
		pService = newServices
	}

	// remove self
	for _, s := range service {
		delete(deps, s)
	}

	return lo.Keys(deps)
}

// serviceDependant build list of services that are dependent on given `services` list.
func (d *dependencyManager) serviceDependant(service ...string) []string {
	deps := make(map[string]struct{}, len(service)*2)
	pService := service[:]
	for len(pService) > 0 {
		var newServices []string
		for _, s := range pService {
			deps[s] = struct{}{}
			srv := d.find(s)
			if srv == nil {
				return nil
			}
			for _, r := range d.findServicesDependOn(s) {
				if _, ok := deps[r]; !ok {
					deps[r] = struct{}{}
					newServices = append(newServices, r)
				}
			}
		}
		pService = newServices
	}

	// remove self
	for _, s := range service {
		delete(deps, s)
	}

	return lo.Keys(deps)
}

func (d *dependencyManager) String() string {
	res := make([]string, len(d.services))
	for i, s := range d.services {
		res[i] = fmt.Sprintf("%s: %v", s.name, s.dependOn)
	}
	return strings.Join(res, "; ")
}
