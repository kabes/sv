package manager

//
// tasks.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/rs/xid"
	"sv.app/internal/services"
	"sv.app/internal/support"
	//	ic "github.com/WAY29/icecream-go/icecream"
)

// taskRunCondition define condition for running task
type taskRunCondition interface {
	fmt.Stringer

	Check(m *Manager) bool
}

// serviceStatusCondition is taskRunCondition that check other services status
type serviceStatusCondition struct {
	serviceName   string
	serviceStatus services.Status
}

func newServiceStatusCondition(service string, status services.Status) *serviceStatusCondition {
	return &serviceStatusCondition{
		serviceName:   service,
		serviceStatus: status,
	}
}

// Check return true when condition met
func (r serviceStatusCondition) Check(m *Manager) bool {
	srv, ok := m.services[r.serviceName]
	return ok && srv.Status() == r.serviceStatus
}

func (r serviceStatusCondition) String() string {
	return fmt.Sprintf("'%s is %s'", r.serviceName, r.serviceStatus)
}

type timeCondition struct {
	ts time.Time
}

func newTimeCondition(delay uint) *timeCondition {
	return &timeCondition{
		ts: time.Now().Add(time.Duration(delay) * time.Second),
	}
}

// Check return true when condition met
func (t *timeCondition) Check(m *Manager) bool {
	return t.ts.Before(time.Now())
}

func (t timeCondition) String() string {
	return fmt.Sprintf("'at %s'", t.ts.Format(time.RFC3339))
}

// task is one scheduled task
type task struct {
	Action *ActionReq
	Req    []taskRunCondition
	ID     xid.ID
}

func newTask(a *ActionReq, reqs []taskRunCondition) *task {
	return &task{Action: a, Req: reqs, ID: xid.New()}
}

// Check is task can be run
func (t *task) Check(m *Manager) (bool, int) {
	if len(t.Req) == 0 {
		return true, 0
	}
	for _, r := range t.Req {
		if !r.Check(m) {
			return false, 0
		}
	}

	return true, len(t.Req)
}

func (t *task) String() string {
	return fmt.Sprintf("Task[%s: %s when %s]",
		t.ID.String(),
		t.Action.String(),
		strings.Join(support.StringlifySlice(t.Req), ","))
}

// IsSingleton check is task is for one service
func (t *task) IsSingleton() bool {
	return len(t.Action.Services) == 1
}

type tasksQueue struct {
	todo []*task

	sync.RWMutex
}

func (t *tasksQueue) append(task *task) {
	t.Lock()
	defer t.Unlock()

	// check duplicates
	for _, ta := range t.todo {
		if ta.Action.Equal(task.Action) {
			return
		}
	}

	t.todo = append(t.todo, task)
}

func (t *tasksQueue) len() int {
	t.RLock()
	defer t.RUnlock()

	return len(t.todo)
}

func (t *tasksQueue) clean() int {
	t.Lock()
	defer t.Unlock()
	queueLen := len(t.todo)
	t.todo = nil
	return queueLen
}

// getNext find next task that is ready to run
func (t *tasksQueue) getNext(m *Manager) *task {
	t.Lock()
	defer t.Unlock()

	if len(t.todo) == 0 {
		return nil
	}

	minPrio := -1
	minIdx := -1

	for idx, item := range t.todo {
		if ok, prio := item.Check(m); ok && (prio < minPrio || minPrio == -1) {
			minPrio = prio
			minIdx = idx
		}
	}

	if minIdx >= 0 {
		item := t.todo[minIdx]
		t.todo = removeSliceItem(t.todo, minIdx)
		return item
	}

	return nil
}

func (t *tasksQueue) status() []string {
	t.RLock()
	defer t.RUnlock()

	res := make([]string, 0, len(t.todo))
	for _, td := range t.todo {
		res = append(res, fmt.Sprintf("%s when %s", td.Action.String(), td.Req))
	}

	return res
}

func removeSliceItem[T any](slice []T, s int) []T {
	return append(slice[:s], slice[s+1:]...)
}

func (t *tasksQueue) servicesWaiting(service ...string) []string {
	t.RLock()
	defer t.RUnlock()

	var res []string
srvloop:
	for _, s := range service {
		for _, td := range t.todo {
			if td.Action.ContainService(s) {
				res = append(res, s)
				continue srvloop
			}
		}
	}

	return res
}
