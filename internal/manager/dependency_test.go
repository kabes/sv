package manager

//
// dependency_test.go
// Copyright (C) 2023 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"sort"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestBuildDependency(t *testing.T) {
	dm := dependencyManager{}

	dm.update("s1", nil)
	dm.update("s2", []string{"s1"})
	dm.update("s3", []string{"s2"})
	dm.update("s4", []string{"s2"})

	if len(dm.services) != 4 {
		t.Errorf("invalid services: %#v", dm.services)
	}

	if s := dm.find("s4"); s == nil {
		t.Errorf("missing s4")
	} else {
		if s.name != "s4" || len(s.dependOn) != 1 || s.dependOn[0] != "s2" {
			t.Errorf("invalid s4: %#v", s)
		}
	}

	dm.update("s4", []string{"s2", "s3"})

	if len(dm.services) != 4 {
		t.Errorf("invalid services: %#v", dm.services)
	}

	if s := dm.find("s4"); s == nil {
		t.Errorf("missing s4")
	} else {
		if s.name != "s4" || len(s.dependOn) != 2 || s.dependOn[0] != "s2" || s.dependOn[1] != "s3" {
			t.Errorf("invalid s4: %#v", s)
		}
	}

	dm.update("s3", []string{"s2"})
	if len(dm.services) != 4 {
		t.Errorf("invalid services: %#v", dm.services)
	}
	if s := dm.find("s3"); s == nil {
		t.Errorf("missing s3")
	} else {
		if s.name != "s3" || len(s.dependOn) != 1 || s.dependOn[0] != "s2" {
			t.Errorf("invalid s3: %#v", s)
		}
	}

	dm.remove("s2")
	if len(dm.services) != 3 {
		t.Errorf("invalid services: %#v", dm.services)
	}
	if s := dm.find("s2"); s != nil {
		t.Errorf("s2 not deleted: %#v", s)
	}

	dm.remove("s1")
	if len(dm.services) != 2 {
		t.Errorf("invalid services: %#v", dm.services)
	}
	if s := dm.find("s1"); s != nil {
		t.Errorf("s1 not deleted: %#v", s)
	}
}

func TestFindDependency(t *testing.T) {
	dm := dependencyManager{}

	dm.update("s1", nil)
	dm.update("s2", []string{"s1"})
	dm.update("s3", []string{"s2"})
	dm.update("s4", []string{"s2", "s3"})

	r := dm.findServicesDependOn("s2")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s3", "s4"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.findServicesDependOn("s1")
	if d := cmp.Diff(r, []string{"s2"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.findServicesDependOn("s3")
	if d := cmp.Diff(r, []string{"s4"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}
}

func TestServiceDependency(t *testing.T) {
	dm := dependencyManager{}

	dm.update("s1", nil)
	dm.update("s2", []string{"s1"})
	dm.update("s3", []string{"s2"})
	dm.update("s4", []string{"s2", "s3"})
	dm.update("s5", []string{"s1"})
	dm.update("s6", nil)
	dm.update("s7", []string{"s4", "s5"})

	r := dm.serviceDependency("s1")
	if len(r) > 0 {
		t.Errorf("invalid dependency: %#v", r)
	}

	r = dm.serviceDependency("s2")
	if d := cmp.Diff(r, []string{"s1"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.serviceDependency("s3")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s1", "s2"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.serviceDependency("s4")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s1", "s2", "s3"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.serviceDependency("s7")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s1", "s2", "s3", "s4", "s5"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}
}

func TestServiceDependencyChanges(t *testing.T) {
	dm := dependencyManager{}

	dm.update("s1", nil)
	dm.update("s2", []string{"s1"})
	dm.update("s3", []string{"s2"})
	dm.update("s4", []string{"s2", "s3"})
	dm.update("s5", []string{"s1"})
	dm.update("s6", nil)
	dm.update("s7", []string{"s4", "s5"})

	r := dm.serviceDependency("s3")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s1", "s2"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	dm.update("s1", []string{"s6"})

	r = dm.serviceDependency("s3")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s1", "s2", "s6"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	dm.update("s2", nil)

	r = dm.serviceDependency("s3")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s2"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}
}

func TestServiceDependant(t *testing.T) {
	dm := dependencyManager{}

	dm.update("s1", nil)
	dm.update("s2", []string{"s1"})
	dm.update("s3", []string{"s2"})
	dm.update("s4", []string{"s2", "s3"})
	dm.update("s5", []string{"s1"})
	dm.update("s6", nil)
	dm.update("s7", []string{"s4", "s5"})

	r := dm.serviceDependant("s6")
	if len(r) > 0 {
		t.Errorf("invalid dependency: %#v", r)
	}

	r = dm.serviceDependant("s5")
	if d := cmp.Diff(r, []string{"s7"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.serviceDependant("s3")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s4", "s7"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.serviceDependant("s4")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s7"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.serviceDependant("s2")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s3", "s4", "s7"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.serviceDependant("s1")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s2", "s3", "s4", "s5", "s7"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}
}

func TestServiceDependantChanges(t *testing.T) {
	dm := dependencyManager{}

	dm.update("s1", nil)
	dm.update("s2", []string{"s1"})
	dm.update("s3", []string{"s2"})
	dm.update("s4", []string{"s2", "s3"})
	dm.update("s5", []string{"s1"})
	dm.update("s6", nil)
	dm.update("s7", []string{"s4", "s5"})

	r := dm.serviceDependant("s3")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s4", "s7"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	dm.remove("s7")

	r = dm.serviceDependant("s3")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s4"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	r = dm.serviceDependant("s2")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s3", "s4"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	dm.update("s1", []string{"s4"})

	r = dm.serviceDependant("s2")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s1", "s3", "s4", "s5"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}

	dm.update("s1", []string{"s2", "s4"})

	r = dm.serviceDependant("s2")
	sort.Strings(r)
	if d := cmp.Diff(r, []string{"s1", "s3", "s4", "s5"}); d != "" {
		t.Errorf("invalid dependency: %s", d)
	}
}
