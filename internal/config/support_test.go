package config

//
// support_test.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"fmt"
	"testing"
)

func TestBuidServicePath(t *testing.T) {
	services := map[string][]string{
		"s1": {},
		"s2": {"s3"},
		"s3": {"s1"},
	}

	res, err := buildServicePath(services)
	if err != nil {
		t.Log("error should be nil", err)
		t.Fail()
	}

	if err := compareSlice(res, []string{"s1", "s3", "s2"}); err != nil {
		t.Log("invalid result", err, res)
		t.Fail()
	}

}

func TestBuidServicePathFail(t *testing.T) {
	services := map[string][]string{
		"s1": {},
		"s2": {"s3"},
		"s3": {"s3"},
	}

	_, err := buildServicePath(services)
	if err == nil {
		t.Log("error should be not nil")
		t.Fail()
	}

}

func TestBuidServicePath2(t *testing.T) {
	services := map[string][]string{
		"s1": {},
		"s2": {"s1"},
		"s3": {"s1"},
	}

	res, err := buildServicePath(services)
	if err != nil {
		t.Log("error should be nil", err)
		t.Fail()
	}

	if err := compareSlice(res, []string{"s1", "s2", "s3"}); err != nil {
		t.Log("invalid result", err, res)
		t.Fail()
	}

}

func TestBuidServicePath3(t *testing.T) {
	services := map[string][]string{
		"s6": {"s5", "s1"},
		"s4": {"s2"},
		"s1": {},
		"s3": {"s1"},
		"s5": {"s3", "s4"},
		"s2": {},
	}

	res, err := buildServicePath(services)
	if err != nil {
		t.Log("error should be nil", err)
		t.Fail()
	}

	exp := []string{"s1", "s2", "s3", "s4", "s5", "s6"}
	if err := compareSlice(res, exp); err != nil {
		t.Log("invalid result", err, res)
		t.Fail()
	}
}

func TestBuidServicePathFail2(t *testing.T) {
	services := map[string][]string{
		"s6": {"s5", "s1"},
		"s4": {"s2"},
		"s1": {},
		"s3": {"s1"},
		"s5": {"s3", "s4", "s6"},
		"s2": {},
	}

	res, err := buildServicePath(services)
	if err == nil {
		t.Log("error should be not nil", err, res)
		t.Fail()
	}
}

func TestBuidServicePathFail3(t *testing.T) {
	services := map[string][]string{
		"s6": {"s5", "s1"},
		"s4": {"s2", "s4"},
		"s1": {},
		"s3": {"s1"},
		"s5": {"s3", "s4"},
		"s2": {},
	}

	res, err := buildServicePath(services)
	if err == nil {
		t.Log("error should be not nil", err, res)
		t.Fail()
	}
}

func compareSlice(a, b []string) error {
	if len(a) != len(b) {
		return fmt.Errorf("invalid length: %d vs %d", len(a), len(b))
	}
	for i, v := range a {
		if v != b[i] {
			return fmt.Errorf("difference on index %d: %v vs %v", i, v, b[i])
		}
	}
	return nil
}
