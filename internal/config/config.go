package config // import "sv.app/config"

import (
	"errors"
	"flag"
	"fmt"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/google/shlex"
	"github.com/knadh/koanf"

	"sv.app/internal/logger"
	"sv.app/internal/os"
)

// Config is global application configuration
type Config struct {
	Debug          bool   `koanf:"debug"`
	ServerAddress  string `koanf:"server_address"`
	MetricsAddress string `koanf:"metrics_address"`

	AutoStartDelay uint `koanf:"autostart_delay"`

	// ConfFiles is list of other configuration files loaded
	ConfFiles []string `koanf:"conf_files"`

	Services map[string]*ServiceConf

	// mainConfFilename is main config file name
	mainConfFilename string

	// flagSet are flags from command line
	flagSet *flag.FlagSet
}

func newConfig(conffile string, flagSet *flag.FlagSet) *Config {
	c := &Config{
		Debug:            false,
		ServerAddress:    "127.0.0.1:7999",
		MetricsAddress:   "127.0.0.1:7998",
		AutoStartDelay:   10,
		Services:         make(map[string]*ServiceConf),
		mainConfFilename: conffile,
		flagSet:          flagSet,
	}
	return c
}

func (c *Config) cloneBase() *Config {
	return &Config{
		Debug:            false,
		ServerAddress:    "127.0.0.1:7999",
		MetricsAddress:   "127.0.0.1:7998",
		AutoStartDelay:   10,
		Services:         make(map[string]*ServiceConf),
		mainConfFilename: c.mainConfFilename,
		flagSet:          c.flagSet,
	}
}

func (c *Config) unmarshal(k *koanf.Koanf) error {

	if err := k.Unmarshal("", &c); err != nil {
		return fmt.Errorf("unmarshal error: %w", err)
	}

	return nil
}

// Changed return true when two ServiceConf is not equal
func (c *Config) Changed(other *Config) bool {
	if logger.Logger.Debug().Enabled() {
		if diff := cmp.Diff(c, other, cmpopts.IgnoreUnexported(Config{})); diff != "" {
			logger.Logger.Debug().Str("diff", diff).Msg("config difference")
			return true
		}
		return false
	}

	return !cmp.Equal(c, other, cmpopts.IgnoreUnexported(Config{}))
}

// validate configuration
func (c *Config) validate() error {
	if len(c.Services) == 0 {
		return errors.New("no services configured")
	}

	for name, srv := range c.Services {
		srv.Name = name
		err := srv.validate()
		if err != nil {
			return fmt.Errorf("validate service %s error: %v", name, err)
		}
	}

	// validate dependencies
	if _, err := BuildServicePath(c.Services); err != nil {
		return fmt.Errorf("dependencies resolve error: %v", err)
	}

	return nil
}

// ServiceType is type of service
type ServiceType string

const (
	// ServiceTypeNormal is standard daemon service when process is running all time
	ServiceTypeNormal ServiceType = "normal"
	// ServiceTypeExt is service that is running by external scripts and main process can exit
	ServiceTypeExt ServiceType = "external"

	// ServiceTypeOnce is service that is not running in background
	ServiceTypeOnce ServiceType = "once"
)

// ServiceConf define one service managed by sv
type ServiceConf struct {
	Name   string
	Groups []string `koanf:"groups"`

	Type ServiceType `koanf:"service_type"`

	Directory   string   `koanf:"directory"`
	Environment []string `koanf:"environment"`

	StartCommand string `koanf:"start_cmd"`
	StartTimeout uint   `koanf:"start_timeout"`

	// StopCommand is used only in ServiceTypeExt
	StopCommand string `koanf:"stop_cmd"`
	StopSignal  string `koanf:"stop_signal"`
	StopTimeout uint   `koanf:"stop_timeout"`

	// ReloadCommand is used only in ServiceTypeExt
	ReloadCommand string `koanf:"reload_cmd"`
	ReloadSignal  string `koanf:"reload_signal"`
	ReloadTimeout uint   `koanf:"reload_timeout"`

	// Start service on sv start
	AutoStart bool `koanf:"auto_start"`

	// TODO: AutoStartDelay

	// Restarts / fails handling
	// AutoRestart when true service is restarted after crash
	AutoRestart bool `koanf:"auto_restart"`
	// RestartDelay is delay before restart
	RestartDelay uint `koanf:"restart_delay,omitempty"`
	// MaxAutoRestart is max numbers of restarts in row
	MaxAutoRestart uint `koanf:"max_auto_restart"`

	// Logging configuration
	// LogStdout is destination for stdout
	LogStdout string `koanf:"log_stdout"`
	// LogStderr is destination for stderr
	LogStderr string `koanf:"log_stderr"`

	// HCConf is health check configurations
	HealthChecks []hcConfig

	// list of services that must be running to start this service
	Require []string `koanf:"require"`
}

func newServiceConf(k *koanf.Koanf) (*ServiceConf, error) {
	serv := &ServiceConf{
		Type:         ServiceTypeNormal,
		StopSignal:   "SIGTERM",
		ReloadSignal: "SIGHUP",
	}

	if err := k.Unmarshal("", serv); err != nil {
		return nil, fmt.Errorf("unmarshal error: %w", err)
	}

	for i, p := range k.Slices("health_checks") {
		hcc, err := newHCConfig(p)
		if err != nil {
			return nil, fmt.Errorf("load healthcheck [%d] error: %w", i, err)
		}
		if hcc != nil {
			serv.HealthChecks = append(serv.HealthChecks, hcc)
		}
	}

	return serv, nil
}

// DependOn check is this service require given `service` to work
func (s *ServiceConf) DependOn(service string) bool {
	for _, r := range s.Require {
		if r == service {
			return true
		}
	}

	return false
}

// Changed return true when two ServiceConf is not equal
func (s *ServiceConf) Changed(other *ServiceConf) bool {
	if logger.Logger.Debug().Enabled() {
		if diff := cmp.Diff(s, other, cmpopts.IgnoreUnexported(ServiceConf{})); diff != "" {
			logger.Logger.Debug().Str("diff", diff).Msg("service conf difference")
			return true
		}
		return false
	}

	return !cmp.Equal(s, other, cmpopts.IgnoreUnexported(ServiceConf{}))
}

// validate Service configuration
func (s *ServiceConf) validate() error {
	if s.Name == "" {
		return errors.New("service name is required")
	}

	if s.Name == "all" {
		return errors.New("service name `all` is prohibited")
	}

	if err := validateCommand(s.StartCommand, true); err != nil {
		return fmt.Errorf("validate start command for service %s error: %v",
			s.Name, err)
	}

	switch s.Type {
	case ServiceTypeNormal:
		if err := validateSignal(s.StopSignal, true); err != nil {
			return fmt.Errorf("validate stop signal service %s error: %v",
				s.Name, err)
		}
		if err := validateSignal(s.ReloadSignal, false); err != nil {
			return fmt.Errorf("validate reload signal service %s error: %v",
				s.Name, err)
		}
	case ServiceTypeExt:
		if err := validateCommand(s.StopCommand, true); err != nil {
			return fmt.Errorf("validate start command for service %s error: %v",
				s.Name, err)
		}
		if err := validateCommand(s.ReloadCommand, false); err != nil {
			return fmt.Errorf("validate start command for service %s error: %v",
				s.Name, err)
		}
	case ServiceTypeOnce:
	}

	// TODO: validate other commands/fields

	for idx, hc := range s.HealthChecks {
		if err := hc.validate(); err != nil {
			return fmt.Errorf("healthcheck configuration for service %s (idx: %d) error: %v",
				s.Name, idx+1, err)
		}
	}

	return nil
}

// validate Command configuration
func validateCommand(cmd string, required bool) error {
	if required && cmd == "" {
		return errors.New("command is required")
	}

	if _, err := shlex.Split(cmd); err != nil {
		return fmt.Errorf("parse command error: %v", err)
	}

	return nil
}

func validateSignal(name string, required bool) error {
	if required && name == "" {
		return errors.New("signal name is required")
	}
	if _, ok := os.GetSignal(name); !ok {
		return fmt.Errorf("unknown signal: %s", name)
	}
	return nil
}
