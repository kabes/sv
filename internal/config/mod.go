package config

import (
	"flag"
	"fmt"
	"log"

	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/toml"
	"github.com/knadh/koanf/providers/basicflag"
	"github.com/knadh/koanf/providers/file"
	"sv.app/internal/logger"
)

func loadConfiguration(conf *Config) error {
	l := logger.Logger

	k := koanf.New(".")

	filename := conf.mainConfFilename
	l.Debug().Msgf("loading config %s", filename)
	if err := k.Load(file.Provider(filename), toml.Parser()); err != nil {
		return fmt.Errorf("read conf file %s error: %w", filename, err)
	}

	// load additional files defined in main config file
	for _, fname := range k.Strings("conf_files") {
		l.Debug().Msgf("loading config %s", fname)
		if err := k.Load(file.Provider(fname), toml.Parser()); err != nil {
			return fmt.Errorf("read conf file %s error: %w", fname, err)
		}
	}

	// load flasg from command line (can overwrite)
	if err := k.Load(basicflag.Provider(conf.flagSet, "."), nil); err != nil {
		log.Fatalf("error loading config from command line: %v", err)
	}

	if e := logger.Logger.Debug(); e.Enabled() {
		e.Msg("conf: " + k.Sprint())
	}

	if err := conf.unmarshal(k); err != nil {
		return fmt.Errorf("load conf error: %w", err)
	}

	for _, servName := range k.MapKeys("services") {
		sk := k.Cut("services." + servName)
		serv, err := newServiceConf(sk)
		if err != nil {
			return fmt.Errorf("read conf for service %s error: %w", servName, err)
		}

		conf.Services[servName] = serv
		logger.Logger.Trace().Str("service", servName).Msgf("conf: %#v", serv)
	}

	return nil
}

// Initialize configuration
func Initialize(f *flag.FlagSet) *Config {
	v := f.Lookup("conf").Value
	cFile := v.String()
	c := newConfig(cFile, f)
	return c
}

// Load load configuration from given file
func Load(c *Config) error {
	err := loadConfiguration(c)
	if err != nil {
		return err
	}

	err = c.validate()
	return err
}

// Reload configuration
func Reload(conf *Config, filename string) (*Config, error) {
	newConf := conf.cloneBase()
	err := Load(newConf)
	if err != nil {
		return nil, err
	}

	if conf.Changed(newConf) {
		return newConf, nil
	}

	return nil, nil
}
