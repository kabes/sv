package config

//
// support.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"fmt"
	"reflect"
	"sort"
)

// BuildServicePath try to build list of services in dependencies order
func BuildServicePath(services map[string]*ServiceConf) ([]string, error) {
	// map[service name][requirements] to process
	defs := make(map[string][]string, len(services))
	for _, s := range services {
		defs[s.Name] = s.Require
	}

	return buildServicePath(defs)
}

func buildServicePath(defs map[string][]string) ([]string, error) {
	// sorted services
	out := make([]string, 0, len(defs))

	// 1. find services without dependencies
	{
		var deleted []string
		for name, reqs := range defs {
			if len(reqs) == 0 {
				deleted = append(deleted, name)
			}
		}
		sort.Strings(deleted)
		for _, name := range deleted {
			out = append(out, name)
			delete(defs, name)
		}
	}

	// 2. process in loop
	for len(defs) > 0 {
		var deleted []string
		for name, reqs := range defs {
			if allInList(out, reqs) {
				deleted = append(deleted, name)
			}
		}
		if len(deleted) == 0 && len(defs) > 0 {
			return nil, fmt.Errorf("cannot build services tree; left: %v", defs)
		}
		sort.Strings(deleted)
		for _, name := range deleted {
			out = append(out, name)
			delete(defs, name)
		}
	}

	return out, nil
}

func inList(list []string, val string) bool {
	for _, v := range list {
		if v == val {
			return true
		}
	}

	return false
}

func allInList(list []string, vals []string) bool {
	for _, v := range vals {
		if !inList(list, v) {
			return false
		}
	}

	return true
}

// setField assign `value` to field `name` in struct `obj`.
// https://stackoverflow.com/posts/26746461/edit
func setField(obj any, name string, value any) error {
	if value == nil {
		return nil
	}

	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)
	val := reflect.ValueOf(value)

	if structFieldValue.Type() != val.Type() {
		return fmt.Errorf("provided value type didn't match obj field type (%T)", value)
	}

	structFieldValue.Set(val)
	return nil
}
