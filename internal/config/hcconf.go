package config

//
// hcconf.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//
import (
	"fmt"
	"regexp"

	//. "github.com/WAY29/icecream-go/icecream"
	"github.com/google/shlex"
	"github.com/knadh/koanf"
)

// HealthCheckType is healthcheck type
type HealthCheckType string

const (
	// HCNamedProcess define healtcheck that look for process by command line
	HCNamedProcess HealthCheckType = "named_process"
	// HCHTTP define healthcheck that check response from http address
	HCHTTP HealthCheckType = "http"
	// HCTCP define healthcheck that check connection to tcp port
	HCTCP HealthCheckType = "tcp"
	// HCScript define healthcheck thath run script and check result code
	HCScript HealthCheckType = "command"
)

type hcConfig interface {
	validate() error
}

// HCScriptConf ic configuration for script-based health checks
type HCScriptConf struct {
	Type        HealthCheckType `koanf:"type"`
	Disabled    bool            `koanf:"disabled"`
	Interval    int             `koanf:"interval"`
	Command     string          `koanf:"command"`
	Timeout     int             `koanf:"timeout"`
	Environment []string        `koanf:"environment"`
	Directory   string          `koanf:"directory"`
}

func (h *HCScriptConf) validate() error {
	if h.Command == "" {
		return fmt.Errorf("missing `command` parameter")
	}
	if _, err := shlex.Split(h.Command); err != nil {
		return fmt.Errorf("parse `command` error: %v", err)
	}
	return nil
}

// HCNamedProcessConf is configuration for named process health check
type HCNamedProcessConf struct {
	Type     HealthCheckType `koanf:"type"`
	Disabled bool            `koanf:"disabled"`
	Interval int             `koanf:"interval"`
	Name     string          `koanf:"name"`
	MinNum   int             `koanf:"min_num"`
}

func (h *HCNamedProcessConf) validate() error {
	if h.Name == "" {
		return fmt.Errorf("missing `name` parameter")
	}

	if _, err := regexp.Compile(h.Name); err != nil {
		return fmt.Errorf("compile `name` error: %v", err)
	}

	if h.MinNum < 0 {
		return fmt.Errorf("invalid `min_num` argument: %#v", h.MinNum)
	}
	return nil
}

// HCTCPConf is configuration for TCP health check
type HCTCPConf struct {
	Type     HealthCheckType `koanf:"type"`
	Disabled bool            `koanf:"disabled"`
	Interval int             `koanf:"interval"`
	Address  string          `koanf:"address"`
	Timeout  int             `koanf:"timeout"`
}

func (h *HCTCPConf) validate() error {
	if h.Address == "" {
		return fmt.Errorf("missing `address` parameter")
	}
	return nil
}

// HCHTTPConf is configuration for HTTP health check
type HCHTTPConf struct {
	Type     HealthCheckType `koanf:"type"`
	Disabled bool            `koanf:"disabled"`
	Interval int             `koanf:"interval"`
	URL      string          `koanf:"url"`
	Timeout  int             `koanf:"timeout"`
}

func (h *HCHTTPConf) validate() error {
	if h.URL == "" {
		return fmt.Errorf("missing `url` parameter")
	}
	return nil
}

func newHCConfig(k *koanf.Koanf) (hcConfig, error) {
	if k.Bool("disabled") {
		return nil, nil
	}

	hcType := k.String("type")
	if hcType == "" {
		return nil, fmt.Errorf("missing type")
	}

	var conf hcConfig
	switch HealthCheckType(hcType) {
	case HCScript:
		conf = &HCScriptConf{}
	case HCNamedProcess:
		conf = &HCNamedProcessConf{}
	case HCTCP:
		conf = &HCTCPConf{}
	case HCHTTP:
		conf = &HCHTTPConf{}
	default:
		return nil, fmt.Errorf("unknown type `%s`", hcType)
	}

	if err := k.Unmarshal("", conf); err != nil {
		return nil, err
	}

	return conf, nil

}
