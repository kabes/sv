package logger

//
// srclogger.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//
// CommonLogger log stdout or stderr for given service name

import (
	"errors"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/rs/zerolog"
)

// ServiceLogger is interface that define logger for service output
type ServiceLogger interface {
	io.Writer

	Open() error

	// Close logger; when is no longer in use; realyClosed=true
	Close() (realyClosed bool, err error)

	// Reopen close current log and reopen it (release file handler)
	Reopen() error
}

// CommonLogger append service logs to application log
type CommonLogger struct {
	Service string
	logger  zerolog.Logger
}

// NewCommonLogger create logger that write to application log
func NewCommonLogger(service string) *CommonLogger {
	return &CommonLogger{
		Service: service,
		logger:  Logger.With().Str("service", service).Logger(),
	}
}

func (p *CommonLogger) Write(b []byte) (n int, err error) {
	p.logger.Info().Msg(string(b))
	return len(b), nil
}

// Close logger (do nothing in this logger)
func (p *CommonLogger) Close() error {
	return nil
}

// Reopen logger (do nothing in this logger)
func (p *CommonLogger) Reopen() error {
	return nil
}

// ServiceFileLogger save service logs in separated file
type ServiceFileLogger struct {
	file *os.File
	name string
	lock sync.Mutex

	stop chan struct{}
	msg  chan []byte

	// clients keep number of sources (commands) that opened this file
	clients int
}

// NewServiceFileLogger create logger for service with `filename` base name for log
func NewServiceFileLogger(filename string) *ServiceFileLogger {
	p := &ServiceFileLogger{
		name: filename,
		file: nil,
		stop: make(chan struct{}),
		msg:  make(chan []byte, 10),
	}
	return p
}

func (p *ServiceFileLogger) writer() {
	for {
		select {
		case <-p.stop:
			return
		case msg := <-p.msg:
			if p.file == nil {
				Logger.Error().Msg("write to closed file")
			} else {
				_, err := p.file.Write(msg)
				if err != nil {
					Logger.Error().Err(err).Msg("write to file error")
				}
			}
		}
	}
}

func (p *ServiceFileLogger) openFile() error {
	if p.file != nil {
		p.stop <- struct{}{}
		if err := p.file.Close(); err != nil {
			Logger.Error().Err(err).Str("filename", p.name).Msg("close file error")
		}
		p.file = nil
	}

	_, err := os.Stat(p.name)
	if err != nil {
		p.file, err = os.Create(p.name)
	} else {
		p.file, err = os.OpenFile(p.name, os.O_RDWR|os.O_APPEND, 0666)
	}

	if err != nil {
		return fmt.Errorf("error opening file %s: %w", p.name, err)
	}

	go p.writer()

	return err
}

func (p *ServiceFileLogger) Write(b []byte) (n int, err error) {
	if p.file == nil {
		return 0, errors.New("write to closed file")
	}

	p.msg <- b

	return len(b), nil
}

// Open logger
func (p *ServiceFileLogger) Open() error {
	p.lock.Lock()
	defer p.lock.Unlock()

	if p.file == nil {
		if err := p.openFile(); err != nil {
			return err
		}
	}

	p.clients++

	Logger.Debug().Str("filename", p.name).Msgf("open file; clients=%d", p.clients)

	return nil
}

// Close logger
func (p *ServiceFileLogger) Close() (bool, error) {
	p.lock.Lock()
	defer p.lock.Unlock()

	if p.file != nil {
		p.clients--
		Logger.Debug().Str("filename", p.name).Msgf("close file; clients=%d", p.clients)
		if p.clients == 0 {
			err := p.file.Close()
			p.file = nil
			close(p.stop)
			return true, err
		}
	} else {
		Logger.Error().Str("filename", p.name).Msgf("close not opened file; clients=%d", p.clients)
		p.clients = 0
	}

	return false, nil
}

// Reopen log file
func (p *ServiceFileLogger) Reopen() error {
	p.lock.Lock()
	defer p.lock.Unlock()

	Logger.Trace().Str("filename", p.name).Msgf("reopen file; clients=%d", p.clients)

	if p.file == nil {
		return fmt.Errorf("can't reopen closed file: %s", p.name)
	}

	return p.openFile()
}

// MarshalZerologObject return formatted ServiceFileLogger for logging
func (p *ServiceFileLogger) MarshalZerologObject(e *zerolog.Event) {
	e.Str("filename", p.name).
		Int("clients", p.clients)
}

// NoopLogger do not log anything
type NoopLogger struct {
}

// NewNoopLogger create new logger that drop all messages
func NewNoopLogger() *NoopLogger {
	return &NoopLogger{}
}

func (p *NoopLogger) Write(b []byte) (n int, err error) {
	return len(b), nil
}

// Close log file (do nothing in this logger)
func (p *NoopLogger) Close() error {
	return nil
}

// Reopen log file (do nothing in this logger)
func (p *NoopLogger) Reopen() error {
	return nil
}
