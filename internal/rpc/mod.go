package rpc

import (
	"sort"
	"time"

	//	. "github.com/WAY29/icecream-go/icecream"

	"github.com/rs/xid"
	"sv.app/internal/manager"
	"sv.app/internal/services"
)

// ActionReq define arguments for action call
type ActionReq struct {
	Action     string
	Arguments  []string
	Parameters map[string]string
	RequestID  xid.ID
}

// ActionResp define response from action call
type ActionResp struct {
	TS     time.Time
	Error  string
	Result []*manager.ActionResult
}

func (a *ActionResp) getSrvActionResults() []*manager.ActionResult {
	sort.Slice(a.Result, func(i1, i2 int) bool {
		return a.Result[i1].Service < a.Result[i2].Service
	})
	return a.Result
}

// StatusResponse is result of status rpc action
type StatusResponse struct {
	TS       time.Time
	Services []*services.StatusInfo
	Queue    []string
	Error    string
}

// GenericResponse is simple response from rpc
type GenericResponse struct {
	TS      time.Time
	Message string
	Error   string
}

// NewGenericResponse create simple response
func NewGenericResponse(msg string, err error) *GenericResponse {
	if err == nil {
		return &GenericResponse{
			TS:      time.Now(),
			Message: msg,
		}
	}

	return &GenericResponse{
		TS:    time.Now(),
		Error: err.Error(),
	}
}

// ServiceEventsResponse is repose for events action
type ServiceEventsResponse struct {
	TS     time.Time
	Error  string
	Events []*manager.EventInfo
}

// ServiceLogsResponse is repose for events action
type ServiceLogsResponse struct {
	TS     time.Time
	Error  string
	Data   []byte
	Offset int64
	EOF    bool
}
