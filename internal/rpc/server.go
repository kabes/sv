package rpc

import (
	"context"
	"errors"
	"io"
	"net"
	"net/http"
	"net/rpc"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/xid"
	"github.com/rs/zerolog"

	"sv.app/internal/config"
	"sv.app/internal/logger"
	"sv.app/internal/manager"
	"sv.app/internal/support"
)

// Server is RPC server
type Server struct {
	conf   *config.Config
	action *Action

	listener net.Listener
}

// metrics
var (
	requestTotal prometheus.Counter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "sv_rpc_request_total",
		Help: "Total number of rpc requests.",
	})
	requestErrorsTotal prometheus.Counter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "sv_rpc_request_errors_total",
		Help: "Total number of rpc requests with error status.",
	})
)

func init() {
	prometheus.MustRegister(requestTotal)
	prometheus.MustRegister(requestErrorsTotal)

}

// NewServer create new Server
func NewServer(manager *manager.Manager, conf *config.Config) *Server {
	return &Server{
		conf: conf,
		action: &Action{
			manager: manager,
		},
	}
}

// Action is action to perform over RPC
type Action struct {
	manager *manager.Manager
}

var reqID uint32 = 0

func createCtxLogger(action string) (zerolog.Logger, context.Context) {
	id := xid.New()
	l := logger.Logger.With().
		Str("reqID", id.String()).
		Str("action", action).
		Logger()

	ctx := context.WithValue(context.Background(), support.CtxRequestID, id)
	ctx = l.WithContext(ctx)

	return l, ctx
}

// Action handle rpc actions
func (a *Action) Action(args ActionReq, res *ActionResp) error {
	l, ctx := createCtxLogger(args.Action)
	l.Info().Interface("args", args).Msg("handle action")

	aa := manager.ActionReq{
		Action:     manager.Action(args.Action),
		Services:   args.Arguments,
		Parameters: args.Parameters,
	}

	var result []*manager.ActionResult
	var err error
	switch aa.Action {
	case manager.ActionReloadLogs:
		result, err = a.manager.ActionReloadLogs(ctx, &aa)
	case manager.ActionSignal:
		result, err = a.manager.ActionSignal(ctx, &aa)
	case manager.ActionRestart:
		result, err = a.manager.ActionRestart(ctx, &aa)
	case manager.ActionReload:
		result, err = a.manager.ActionReload(ctx, &aa)
	default:
		result, err = a.manager.Action(ctx, &aa)
	}

	if err != nil {
		requestErrorsTotal.Inc()
		l.Error().Err(err).Msg("action error")
		*res = ActionResp{
			Error:  err.Error(),
			Result: nil,
		}
	} else {
		l.Info().Interface("result", result).Msg("action result")
		*res = ActionResp{
			Error:  "",
			Result: result,
		}
	}

	return nil
}

// ServiceEvents get events from services
func (a *Action) ServiceEvents(args *ActionReq, resp *ServiceEventsResponse) error {
	l, ctx := createCtxLogger(args.Action)
	l.Info().Interface("args", args).Msg("handle ServiceEvents")

	aa := manager.ActionReq{
		Action:     manager.Action(args.Action),
		Services:   args.Arguments,
		Parameters: args.Parameters,
	}
	if r, err := a.manager.ActionEvents(ctx, &aa); err == nil {
		*resp = ServiceEventsResponse{
			TS:     time.Now(),
			Events: r,
		}
	} else {
		requestErrorsTotal.Inc()
		*resp = ServiceEventsResponse{
			TS:    time.Now(),
			Error: err.Error(),
		}
	}
	return nil
}

// ServiceLogs get logs from services
func (a *Action) ServiceLogs(args *ActionReq, resp *ServiceLogsResponse) error {
	l, ctx := createCtxLogger(args.Action)
	l.Info().Interface("args", args).Msg("handle ServiceEvents")

	aa := manager.ActionReq{
		Action:     manager.Action(args.Action),
		Services:   args.Arguments,
		Parameters: args.Parameters,
	}

	buf := make([]byte, 1024)

	offset, err := a.manager.ActionReadLogs(ctx, &aa, &buf)
	l.Debug().Int64("offset", offset).Err(err).Msg("ActionReadLogs result")
	if err == nil || errors.Is(err, io.EOF) {
		*resp = ServiceLogsResponse{
			TS:     time.Now(),
			Data:   buf,
			Offset: offset,
			EOF:    errors.Is(err, io.EOF),
		}
		return nil
	}

	requestErrorsTotal.Inc()
	*resp = ServiceLogsResponse{
		TS:    time.Now(),
		Error: err.Error(),
	}

	return nil

}

// ManagerStatus get status of manager & services
func (a *Action) ManagerStatus(_ struct{}, ms *manager.Status) error {
	l, ctx := createCtxLogger("manager_status")
	l.Info().Msg("handle ManagerStatus")

	r, err := a.manager.ManagerStatus(ctx)
	if err != nil {
		requestErrorsTotal.Inc()
		return err
	}
	*ms = *r
	return nil
}

// ManagerShutdown get status of manager & services
func (a *Action) ManagerShutdown(_ struct{}, ms *GenericResponse) error {
	l, ctx := createCtxLogger("manager_shutdown")
	l.Info().Msg("handle ManagerShutdown")

	r, err := a.manager.ManagerShutdown(ctx)
	if err != nil {
		requestErrorsTotal.Inc()
	}
	*ms = *NewGenericResponse(r, err)
	return nil
}

// ManagerAbort clear tasks queue
func (a *Action) ManagerAbort(_ struct{}, ms *GenericResponse) error {
	l, ctx := createCtxLogger("manager_abort")
	l.Info().Msg("handle ManagerAbort")

	r, err := a.manager.ManagerAbort(ctx)
	if err != nil {
		requestErrorsTotal.Inc()
	}
	*ms = *NewGenericResponse(r, err)
	return nil
}

// ManagerReload clear tasks queue
func (a *Action) ManagerReload(_ struct{}, ms *GenericResponse) error {
	l, ctx := createCtxLogger("manager_reload")
	l.Info().Msg("handle ManagerReload")

	r, err := a.manager.ManagerReload(ctx, nil)
	if err != nil {
		requestErrorsTotal.Inc()
	}
	*ms = *NewGenericResponse(r, err)
	return nil
}

// Status handle rpc status actions
func (a *Action) Status(args ActionReq, res *StatusResponse) error {
	l, ctx := createCtxLogger(args.Action)
	l.Info().Interface("args", args).Msg("handle action")

	aa := manager.ActionReq{
		Action:     "status",
		Services:   args.Arguments,
		Parameters: args.Parameters,
	}

	statuses, queue, err := a.manager.ActionStatus(ctx, &aa)
	if err != nil {
		l.Error().Err(err).Msg("action error")
		requestErrorsTotal.Inc()
		*res = StatusResponse{
			Error: err.Error(),
			TS:    time.Now(),
		}
		return nil
	}

	l.Info().Interface("statuses", statuses).Strs("queue", queue).Msg("action result")
	*res = StatusResponse{
		TS:       time.Now(),
		Services: statuses,
		Queue:    queue,
	}

	return nil
}

// PID handle rpc status actions
func (a *Action) PID(args ActionReq, res *ActionResp) error {
	l, _ := createCtxLogger(args.Action)

	aa := manager.ActionReq{
		Action:     "status",
		Services:   args.Arguments,
		Parameters: args.Parameters,
	}

	result, err := a.manager.ActionPID(l.WithContext(context.Background()), &aa)
	if err != nil {
		requestErrorsTotal.Inc()
		l.Error().Err(err).Msg("action error")
		*res = ActionResp{
			Error:  err.Error(),
			Result: nil,
		}
	} else {
		l.Info().Interface("result", result).Msg("action result")
		*res = ActionResp{
			Error:  "",
			Result: result,
		}
	}

	return nil
}

// Help return information about available commands
func (a *Action) Help(_ struct{}, ms *string) error {
	*ms = (`
Commands:
   abort        clear waiting tasks queue
   events [<services>]
                print last events
   logs <service> [--tail] [--output=out|err|stdout|stderr]
                print last service log entries
   pid [<services>] [--short]
                list process pid for selected or all services
   reload <service>
                 reload service (send signal, run reload script)
   reload-conf reload configuration
   reload-logs [<services>]
                close and open log files
   restart <services> [--wait] [--no-dep]
                restart service or services
   shutdown     stop all services and shutdown sv
   signal <services> --signal=<signal>
                send signal to service; argument required
   status [<services>] [-v]
                return status of service/services
   start <services> [--wait] [--no-dep]
                start service or services
   stop <services> [--wait] [--no-dep]
                stop service or services
`)
	return nil
}

// Start RPC server
func (s *Server) Start() error {
	log := logger.Logger
	log.Info().Str("address", s.conf.ServerAddress).Msg("starting rpc server")

	rpc.Register(s.action)
	http.Handle(rpc.DefaultRPCPath, s)

	//rpc.HandleHTTP()
	l, e := net.Listen("tcp", s.conf.ServerAddress)
	if e != nil {
		log.Fatal().Err(e).Msg("listen error")
		return e
	}
	s.listener = l
	return http.Serve(l, nil)
}

func (s *Server) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	requestTotal.Inc()
	rpc.DefaultServer.ServeHTTP(w, req)
}

// Stop RPC server
func (s *Server) Stop() {
	s.listener.Close()
}
