package rpc

import (
	"context"
	"fmt"
	"net/rpc"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/rs/xid"
	"github.com/rs/zerolog"
	"github.com/samber/lo"

	"sv.app/internal/config"
	"sv.app/internal/logger"
	"sv.app/internal/manager"
	"sv.app/internal/support"
)

// Client is RPC client
type Client struct {
	Conf *config.Config
}

// Call action over RPC
func (c *Client) Call(action string, args []string) error {
	requestID := xid.New()
	l := logger.Logger.With().Str("subsytem", "rpc_cli").
		Stringer("client_req_id", requestID).
		Str("action", action).Interface("args", args).Logger()
	l.Info().Msg("client call")

	ctx := l.WithContext(context.Background())
	client, err := rpc.DialHTTP("tcp", c.Conf.ServerAddress)
	if err != nil {
		l.Error().Err(err).Msg("dial error")
		return err
	}
	defer client.Close()

	rpcarg := createReq(action, args, requestID)

	switch action {
	case "help":
		return callActionHelp(ctx, client, rpcarg)
	case "status":
		return callActionStatus(ctx, client, rpcarg)
	case "shutdown":
		return callManagerShutdown(ctx, client, rpcarg)
	case "abort":
		return callManagerAbort(ctx, client, rpcarg)
	case "events":
		return callActionEvents(ctx, client, rpcarg)
	case "reload-conf":
		return callManagerReload(ctx, client, rpcarg)
	case "logs":
		return callActionLogs(ctx, client, rpcarg)
	case "pid":
		return callActionPID(ctx, client, rpcarg)
	}

	var res ActionResp
	if err = client.Call("Action.Action", rpcarg, &res); err != nil {
		l.Error().Err(err).Interface("res", res).Msg("call result error")
		return fmt.Errorf("error call rpc: %w", err)
	}

	l.Debug().Interface("res", res).Msg("call result")

	if res.Error != "" {
		fmt.Fprintf(os.Stderr, "Error: %s", res.Error)
		return nil
	}

	// print result - services
	for _, res := range res.getSrvActionResults() {
		l.Debug().Interface("res", res).Msg("srv res")
		if res.Error != "" {
			fmt.Printf("ERR: %-20s %s\n", res.Service, res.Error)
		} else {
			fmt.Printf("OK:  %-20s %s\n", res.Service, res.Data)
		}
	}

	if _, ok := rpcarg.Parameters["--wait"]; ok {
		waitForFinish(ctx, client)
	}

	return nil
}

func createReq(action string, args []string, requestID xid.ID) *ActionReq {
	rpcarg := &ActionReq{
		Action:     action,
		Arguments:  nil,
		Parameters: make(map[string]string),
		RequestID:  requestID,
	}

	for _, arg := range args {
		arg = strings.TrimSpace(arg)
		if len(arg) == 0 {
			continue
		}

		if arg[0] == '-' {
			if ps := strings.SplitN(arg, "=", 2); len(ps) == 1 {
				rpcarg.Parameters[ps[0]] = ""
			} else {
				rpcarg.Parameters[ps[0]] = ps[1]
			}
		} else {
			rpcarg.Arguments = append(rpcarg.Arguments, arg)
		}
	}

	return rpcarg
}

func waitForFinish(ctx context.Context, client *rpc.Client) {
	l := zerolog.Ctx(ctx)
	fmt.Println("waiting...")
	timeout := 300

	ctx, cancel := context.WithTimeout(ctx, time.Duration(timeout)*time.Second)
	defer cancel()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	statuses := make(map[string]string)
	for {
		select {
		case <-ctx.Done():
			fmt.Println("timeout")
			return
		case <-ticker.C:
			ms, err := checkManagerStatus(ctx, client)
			if err != nil {
				l.Error().Err(err).Msg("check service status error")
				fmt.Printf("ERR: %s\n", err.Error())
				return
			}

			for _, msg := range getStatusesDiff(statuses, ms.ServiceStatus) {
				fmt.Printf("   %s\n", msg)
			}
			statuses = ms.ServiceStatus

			if len(ms.Queue) == 0 {
				fmt.Println("OK: Done")
				return
			}
		}
	}
}

func checkManagerStatus(ctx context.Context, client *rpc.Client) (*manager.Status, error) {
	var res manager.Status

	if err := client.Call("Action.ManagerStatus", struct{}{}, &res); err != nil {
		return nil, fmt.Errorf("error call rpc: %w", err)
	}

	return &res, nil
}

func getStatusesDiff(current, updated map[string]string) []string {
	return support.FilterMapToSlice(updated, func(n, s string) (string, bool) {
		prev, ok := current[n]
		if ok && prev == s {
			return "", false
		}
		return fmt.Sprintf("%-15s %s", n, s), true
	})
}

func callActionStatus(ctx context.Context, client *rpc.Client, args *ActionReq) error {
	l := zerolog.Ctx(ctx)

	for {
		var resp StatusResponse
		if err := client.Call("Action.Status", args, &resp); err != nil {
			l.Error().Err(err).Interface("resp", resp).Msg("call result error")
			return fmt.Errorf("error call rpc: %w", err)
		}

		l.Debug().Interface("resp", resp).Msg("call result")

		if resp.Error != "" {
			fmt.Printf("ERR: %s\n", resp.Error)
			return nil
		}

		sort.Slice(resp.Services, func(i1, i2 int) bool {
			return resp.Services[i1].Service < resp.Services[i2].Service
		})

		for _, s := range resp.Services {
			fmt.Printf("%-20s%s\n", s.Service, s.StatusHuman())
			dkeys := lo.Keys(s.Data)
			sort.Strings(dkeys)
			for _, k := range dkeys {
				d := s.Data[k]
				k = strings.ReplaceAll(k, "_", " ")
				fmt.Printf("                                %-20s%s\n", k+":", d)
			}
		}

		if len(resp.Queue) == 0 {
			fmt.Printf("\nManager queue: empty\n")
		} else {
			fmt.Printf("\nManager queue:\n")
			for _, q := range resp.Queue {
				fmt.Printf("               %s\n", q)
			}
		}

		if w, ok := args.Parameters["--watch"]; ok {
			wait := 1
			if w != "" {
				if w, err := strconv.Atoi(w); err == nil {
					wait = w
				}
			}
			time.Sleep(time.Duration(wait) * time.Second)
			fmt.Println()
			continue
		}

		break
	}

	return nil
}

func callActionLogs(ctx context.Context, client *rpc.Client, args *ActionReq) error {
	l := zerolog.Ctx(ctx)

	offset := int64(0)
	_, tail := args.Parameters["--tail"]

	for {
		args.Parameters["offset"] = strconv.FormatInt(offset, 10)

		var resp ServiceLogsResponse
		if err := client.Call("Action.ServiceLogs", args, &resp); err != nil {
			l.Error().Err(err).Interface("resp", resp).Msg("call result error")
			return fmt.Errorf("error call rpc: %w", err)
		}
		l.Debug().Interface("resp", resp).Msg("call result")

		if resp.Error != "" {
			fmt.Printf("ERR: %s\n", resp.Error)
			break
		}

		fmt.Print(string(resp.Data))
		if resp.Offset > offset {
			offset = resp.Offset
		}

		if resp.EOF {
			if !tail {
				break
			}
			time.Sleep(time.Duration(1) * time.Second)
		}
	}
	return nil
}

func callActionEvents(ctx context.Context, client *rpc.Client, args *ActionReq) error {
	l := zerolog.Ctx(ctx)

	var resp ServiceEventsResponse
	if err := client.Call("Action.ServiceEvents", args, &resp); err != nil {
		l.Error().Err(err).Interface("resp", resp).Msg("call result error")
		return fmt.Errorf("error call rpc: %w", err)
	}

	l.Debug().Interface("resp", resp).Msg("call result")

	if len(resp.Events) == 0 {
		fmt.Printf("no events")
		return nil
	}

	for _, e := range resp.Events {
		fmt.Printf("%s %-20s [%1s] %s\n", e.TS.Format(time.RFC3339), e.Object, e.Type, e.Message)
	}

	return nil
}

func callManagerShutdown(ctx context.Context, client *rpc.Client, args *ActionReq) error {
	return callGeneric(ctx, client, "Action.ManagerShutdown", args)
}

func callManagerAbort(ctx context.Context, client *rpc.Client, args *ActionReq) error {
	return callGeneric(ctx, client, "Action.ManagerAbort", args)
}

func callManagerReload(ctx context.Context, client *rpc.Client, args *ActionReq) error {
	return callGeneric(ctx, client, "Action.ManagerReload", args)
}

func callGeneric(ctx context.Context, client *rpc.Client, method string, args *ActionReq) error {
	l := zerolog.Ctx(ctx)

	var resp GenericResponse
	if err := client.Call(method, args, &resp); err != nil {
		l.Error().Err(err).Interface("resp", resp).Msg("call result error")
		return fmt.Errorf("error call rpc: %w", err)
	}

	l.Debug().Interface("resp", resp).Msg("call result")

	if resp.Error == "" {
		fmt.Printf("OK:  %s\n", resp.Message)
	} else {
		fmt.Printf("ERR: %s\n", resp.Message)
	}
	return nil
}

func callActionHelp(ctx context.Context, client *rpc.Client, args *ActionReq) error {
	l := zerolog.Ctx(ctx)

	var resp string
	if err := client.Call("Action.Help", args, &resp); err != nil {
		l.Error().Err(err).Interface("resp", resp).Msg("call result error")
		return fmt.Errorf("error call rpc: %w", err)
	}

	fmt.Println(resp)

	return nil
}

func callActionPID(ctx context.Context, client *rpc.Client, args *ActionReq) error {
	l := zerolog.Ctx(ctx)
	var resp ActionResp
	if err := client.Call("Action.PID", args, &resp); err != nil {
		l.Error().Err(err).Interface("resp", resp).Msg("call result error")
		return fmt.Errorf("error call rpc: %w", err)
	}

	l.Debug().Interface("resp", resp).Msg("call result")

	if resp.Error != "" {
		fmt.Printf("ERR: %s\n", resp.Error)
		return nil
	}

	sort.Slice(resp.Result, func(i1, i2 int) bool {
		return resp.Result[i1].Service < resp.Result[i2].Service
	})

	if _, s := args.Parameters["--short"]; s {
		for _, s := range resp.Result {
			if s.Data != nil {
				fmt.Printf("%d\n", s.Data.(int))
			}
		}
	} else {
		for _, s := range resp.Result {
			if s.Error != "" {
				fmt.Printf("%-20s%s\n", s.Service, s.Error)
			} else {
				fmt.Printf("%-20s%d\n", s.Service, s.Data.(int))
			}
		}
	}

	return nil
}
