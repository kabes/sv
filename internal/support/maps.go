package support

//
// maps.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

// CompareMaps return true when two maps are equal
func CompareMaps[T comparable, V comparable](map1, map2 map[T]V) bool {
	if len(map1) != len(map2) {
		return false
	}
	for k := range map1 {
		val2, ok := map2[k]
		val1 := map1[k]
		if !ok || val1 != val2 {
			return false
		}
	}

	return true
}

// FilterMapToSlice transforms a map into a slice based on specific callback.
// The callback function should return two values:
//   - the result of the mapping operation and
//   - whether the result element should be included or not.
//
// based on https://github.com/samber/lo
func FilterMapToSlice[K comparable, V any, R any](in map[K]V, callback func(key K, value V) (R, bool)) []R {
	result := make([]R, 0, len(in))

	for k, v := range in {
		if nv, ok := callback(k, v); ok {
			result = append(result, nv)
		}
	}

	return result
}
