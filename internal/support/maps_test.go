package support

import "testing"

//
// maps_test.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

func TestCompareMaps(t *testing.T) {
	m1 := map[string]int{"aa": 1, "bb": 2}
	m2 := map[string]int{"aa": 1, "bb": 2}
	if !CompareMaps(m1, m2) {
		t.Fatalf("maps should be equal")
	}

	m2 = map[string]int{"aa": 1, "bc": 2}
	if CompareMaps(m1, m2) {
		t.Fatalf("maps should not be equal")
	}

	m2 = map[string]int{"aa": 1, "bc": 2, "bb": 2}
	if CompareMaps(m1, m2) {
		t.Fatalf("maps should not be equal")
	}

	m2 = map[string]int{}
	if CompareMaps(m1, m2) {
		t.Fatalf("maps should not be equal")
	}
}
