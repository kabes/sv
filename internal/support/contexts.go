package support

//
// contexts.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

type contextKey int

const (
	// CtxRequestID is key of request id in cotnext
	CtxRequestID contextKey = iota
)
