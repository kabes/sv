package support

import "fmt"

//
// slices.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

// CompareSlice return true when two slices are the same
func CompareSlice[T comparable](s1, s2 []T) bool {
	if len(s1) != len(s2) {
		return false
	}
	for i, v := range s1 {
		if v != s2[i] {
			return false
		}
	}

	return true
}

// AssociateIdx returns a map containing key-value pairs provided by transform function applied to elements of the given slice.
// If any of two pairs would have the same key the last one gets added to the map.
// The order of keys in returned map is not specified and is not guaranteed to be the same from the original array.
// based on https://github.com/samber/lo/
func AssociateIdx[T any, K comparable, V any](collection []T, transform func(item T, n int) (K, V)) map[K]V {
	result := make(map[K]V)

	for n, t := range collection {
		k, v := transform(t, n)
		result[k] = v
	}

	return result
}

// CompareSliceShallow check if two slices contains the same items
func CompareSliceShallow[T comparable](s1, s2 []T) bool {
	if len(s1) != len(s2) {
		return false
	}

	for i, s := range s1 {
		if s2[i] != s {
			return false
		}
	}

	return true
}

// StringlifySlice create slice of string from slice of Stringer
func StringlifySlice[T fmt.Stringer](slice []T) []string {
	res := make([]string, len(slice))
	for i, s := range slice {
		res[i] = s.String()
	}
	return res
}

// First return first item from collection for which predicate return true.
func First[T any](collection []T, predicate func(item T) bool) (T, bool) {
	for _, i := range collection {
		if predicate(i) {
			return i, true
		}
	}

	var result T
	return result, false
}

// RemoveFirstItem return collection without first occurrence of item.
func RemoveFirstItem[T comparable](collection []T, item T) []T {
	for idx, i := range collection {
		if i == item {
			return append(collection[:idx], collection[idx+1:]...)
		}
	}
	return collection
}

// Remove item from slice by index; return copy of slice return collection without first occurrence of item.
func Remove[T any](collection []T, index int) []T {
	return append(collection[:index], collection[index+1:]...)
}
