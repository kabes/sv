package support

//
// broker.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

// https://stackoverflow.com/a/49877632

import (
	"github.com/rs/zerolog"
	"sv.app/internal/logger"
)

// Broker is pub-sub async communication manager
type Broker struct {
	stopCh    chan struct{}
	publishCh chan interface{}
	subCh     chan chan interface{}
	unsubCh   chan chan interface{}
	log       zerolog.Logger
	alive     bool
}

// NewBroker create new broker
func NewBroker() *Broker {
	return &Broker{
		stopCh:    make(chan struct{}),
		publishCh: make(chan interface{}, 1),
		subCh:     make(chan chan interface{}, 1),
		unsubCh:   make(chan chan interface{}, 1),
		log:       logger.Logger.With().Str("subsystem", "broker").Logger(),
		alive:     true,
	}
}

// NewBrokerStarted create and start new broker
func NewBrokerStarted() *Broker {
	br := NewBroker()
	go br.Start()
	return br
}

// Start broker background process
func (b *Broker) Start() error {
	b.log.Debug().Msg("broker started")
	subs := map[chan interface{}]struct{}{}
	for {
		select {
		case <-b.stopCh:
			b.stopCh = nil
			b.alive = false
			b.log.Debug().Msg("broker stopped")
			return nil
		case msgCh := <-b.subCh:
			subs[msgCh] = struct{}{}
		case msgCh := <-b.unsubCh:
			delete(subs, msgCh)
		case msg := <-b.publishCh:
			for msgCh := range subs {
				// msgCh is buffered, use non-blocking send to protect the broker:
				select {
				case msgCh <- msg:
				default:
				}
			}
		}
	}
}

// Stop broker and its background process
func (b *Broker) Stop() {
	b.log.Debug().Msg("stopping broker")
	if b.stopCh != nil {
		close(b.stopCh)
	}
}

// Subscribe create new channel for subscriber
func (b *Broker) Subscribe() chan interface{} {
	if !b.alive {
		return nil
	}
	msgCh := make(chan interface{}, 10)
	b.subCh <- msgCh
	return msgCh
}

// Unsubscribe remove subscriber channel
func (b *Broker) Unsubscribe(msgCh chan interface{}) {
	if b.alive {
		b.unsubCh <- msgCh
	}
}

// Publish send `msg` to all subscribers
func (b *Broker) Publish(msg interface{}) {
	if b.alive {
		b.log.Trace().Interface("msg", msg).Msg("publish")
		b.publishCh <- msg
	}
}
