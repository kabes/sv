package instrumentation

//
// prometheus.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"net"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"sv.app/internal/config"
	"sv.app/internal/logger"
	"sv.app/internal/manager"
)

// MetricsServer is http server that handle metrics
type MetricsServer struct {
	conf    *config.Config
	manager *manager.Manager

	listener net.Listener
}

// NewMetricsServer create new MetricsServer
func NewMetricsServer(manager *manager.Manager, conf *config.Config) *MetricsServer {

	prometheus.DefaultRegisterer.Register(newManagerStatsCollector(manager))

	return &MetricsServer{
		conf:    conf,
		manager: manager,
	}
}

// Start metrics server
func (s *MetricsServer) Start() error {
	log := logger.Logger
	log.Info().Str("address", s.conf.MetricsAddress).Msg("starting rpc server")

	l, e := net.Listen("tcp", s.conf.MetricsAddress)
	if e != nil {
		log.Fatal().Err(e).Msg("listen error")
		return e
	}
	s.listener = l
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/health", func(rw http.ResponseWriter, r *http.Request) {
		rw.WriteHeader(200)
		rw.Write([]byte("ok"))
	})
	return http.Serve(l, nil)
}

// Stop metrics server
func (s *MetricsServer) Stop() {
	s.listener.Close()
}

type managerStatsCollector struct {
	manager *manager.Manager

	serviceStatus      *prometheus.Desc
	serviceStartUnix   *prometheus.Desc
	serviceStarts      *prometheus.Desc
	serviceErrors      *prometheus.Desc
	serviceHcErrors    *prometheus.Desc
	serviceConfPending *prometheus.Desc
}

func newManagerStatsCollector(manager *manager.Manager) prometheus.Collector {
	return &managerStatsCollector{
		manager: manager,
		serviceStatus: prometheus.NewDesc(
			"sv_service_status",
			"Service status",
			[]string{"service", "status"},
			nil,
		),
		serviceStartUnix: prometheus.NewDesc(
			"sv_service_start_unix",
			"Service status",
			[]string{"service"},
			nil,
		),
		serviceStarts: prometheus.NewDesc(
			"sv_service_starts_total",
			"Total number of service starts",
			[]string{"service"},
			nil,
		),
		serviceErrors: prometheus.NewDesc(
			"sv_service_errors_total",
			"Total number of errors for service",
			[]string{"service"},
			nil,
		),
		serviceHcErrors: prometheus.NewDesc(
			"sv_service_hc_errors_total",
			"Total number of healtcheck errors for service",
			[]string{"service"},
			nil,
		),
		serviceConfPending: prometheus.NewDesc(
			"sv_service_conf_pending",
			"Is configuration waiting for reload",
			[]string{"service"},
			nil,
		),
	}
}

// Describe implements Collector.
func (c *managerStatsCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.serviceStatus
}

// Collect implements Collector.
func (c *managerStatsCollector) Collect(ch chan<- prometheus.Metric) {
	c.manager.CollectMetrics(func(m *manager.Metrics) {
		if m.Broken {
			ch <- prometheus.MustNewConstMetric(
				c.serviceStatus, prometheus.GaugeValue, float64(1), m.Name, "broken",
			)
		} else {
			ch <- prometheus.MustNewConstMetric(
				c.serviceStatus, prometheus.GaugeValue, float64(1), m.Name, m.Status,
			)
			ch <- prometheus.MustNewConstMetric(
				c.serviceStartUnix, prometheus.CounterValue, float64(m.StartTS), m.Name,
			)
		}
		ch <- prometheus.MustNewConstMetric(
			c.serviceStarts, prometheus.CounterValue, float64(m.Starts), m.Name,
		)
		ch <- prometheus.MustNewConstMetric(
			c.serviceErrors, prometheus.CounterValue, float64(m.TotalErrors), m.Name,
		)
		ch <- prometheus.MustNewConstMetric(
			c.serviceHcErrors, prometheus.CounterValue, float64(m.HCTotalErrors), m.Name,
		)
		cp := 0.0
		if m.ConfPending {
			cp = 1.0
		}
		ch <- prometheus.MustNewConstMetric(
			c.serviceConfPending, prometheus.CounterValue, cp, m.Name)
	})
}
