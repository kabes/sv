//go:build windows
// +build windows

//
// reaper_win.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

package os

// StartReaper start process (grim) reaper for cleaning up zombie processes
func StartReaper() {
	// do nothing on win
}
