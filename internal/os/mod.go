package os

import (
	"strings"
	"syscall"
)

//
// mod.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

// GetSignal return signal id from name
func GetSignal(signame string) (syscall.Signal, bool) {
	if signame == "" {
		return 0, false
	}
	signame = strings.ToUpper(signame)

	signal, ok := signals[signame]
	if ok {
		return signal, true
	}

	signal, ok = signals["SIG"+signame]
	return signal, ok
}
