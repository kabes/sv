//go:build !windows && !darwin
// +build !windows,!darwin

package os

//
// signals.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"syscall"
)

// Signals map signal name to signal
var signals map[string]syscall.Signal

func init() {
	signals = make(map[string]syscall.Signal)

	signals["SIGABRT"] = syscall.SIGABRT
	signals["SIGALRM"] = syscall.SIGALRM
	signals["SIGBUS"] = syscall.SIGBUS
	signals["SIGCHLD"] = syscall.SIGCHLD
	signals["SIGCLD"] = syscall.SIGCLD
	signals["SIGCONT"] = syscall.SIGCONT
	signals["SIGFPE"] = syscall.SIGFPE
	signals["SIGHUP"] = syscall.SIGHUP
	signals["SIGILL"] = syscall.SIGILL
	signals["SIGINT"] = syscall.SIGINT
	signals["SIGIO"] = syscall.SIGIO
	signals["SIGIOT"] = syscall.SIGIOT
	signals["SIGKILL"] = syscall.SIGKILL
	signals["SIGPIPE"] = syscall.SIGPIPE
	signals["SIGPOLL"] = syscall.SIGPOLL
	signals["SIGPROF"] = syscall.SIGPROF
	signals["SIGPWR"] = syscall.SIGPWR
	signals["SIGQUIT"] = syscall.SIGQUIT
	signals["SIGSEGV"] = syscall.SIGSEGV
	signals["SIGSTKFLT"] = syscall.SIGSTKFLT
	signals["SIGSTOP"] = syscall.SIGSTOP
	signals["SIGSYS"] = syscall.SIGSYS
	signals["SIGTERM"] = syscall.SIGTERM
	signals["SIGTRAP"] = syscall.SIGTRAP
	signals["SIGTSTP"] = syscall.SIGTSTP
	signals["SIGTTIN"] = syscall.SIGTTIN
	signals["SIGTTOU"] = syscall.SIGTTOU
	signals["SIGUNUSED"] = syscall.SIGUNUSED
	signals["SIGURG"] = syscall.SIGURG
	signals["SIGUSR1"] = syscall.SIGUSR1
	signals["SIGUSR2"] = syscall.SIGUSR2
	signals["SIGVTALRM"] = syscall.SIGVTALRM
	signals["SIGWINCH"] = syscall.SIGWINCH
	signals["SIGXCPU"] = syscall.SIGXCPU
	signals["SIGXFSZ"] = syscall.SIGXFSZ
}

// GetSysProcAttr return SysProcAttr for given os
func GetSysProcAttr() *syscall.SysProcAttr {
	return &syscall.SysProcAttr{
		Setpgid:   true,
		Pdeathsig: syscall.SIGKILL,
	}
}
