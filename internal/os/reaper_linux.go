//go:build !windows && !darwin
// +build !windows,!darwin

//
// reaper_linux.go
// Copyright (C) 2022 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

package os

import (
	reaper "github.com/ramr/go-reaper"
	"sv.app/internal/logger"
)

// StartReaper start process (grim) reaper for cleaning up zombie processes
func StartReaper() {
	logger.Logger.Info().Str("subsystem", "reaper").Msg("starting reaper")
	go reaper.Reap()
}
