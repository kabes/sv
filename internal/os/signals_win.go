//go:build windows
// +build windows

package os

//
// signals.go
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
//
// Distributed under terms of the GPLv3 license.
//

import (
	"syscall"
)

// Signals map signal name to signal
var signals map[string]syscall.Signal

func init() {
	signals = make(map[string]syscall.Signal)

	signals["SIGABRT"] = syscall.SIGABRT
	signals["SIGALRM"] = syscall.SIGALRM
	signals["SIGBUS"] = syscall.SIGBUS
	signals["SIGFPE"] = syscall.SIGFPE
	signals["SIGHUP"] = syscall.SIGHUP
	signals["SIGILL"] = syscall.SIGILL
	signals["SIGINT"] = syscall.SIGINT
	signals["SIGKILL"] = syscall.SIGKILL
	signals["SIGPIPE"] = syscall.SIGPIPE
	signals["SIGQUIT"] = syscall.SIGQUIT
	signals["SIGSEGV"] = syscall.SIGSEGV
	signals["SIGTERM"] = syscall.SIGTERM
	signals["SIGTRAP"] = syscall.SIGTRAP
}

// GetSysProcAttr return SysProcAttr for given os
func GetSysProcAttr() *syscall.SysProcAttr {
	return nil
}
